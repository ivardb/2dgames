package boardgames;

import boardgames.gui.chess.ChessPanel;
import boardgames.gui.sudoku.panels.SudokuSelectorPanel;
import java.util.function.Supplier;
import javax.swing.JPanel;
import lombok.Getter;

@Getter
public enum Game {

    CHESS("Chess", "chess.png", ChessPanel::new),
    SUDOKU("Sudoku", "sudoku.png", SudokuSelectorPanel::new);

    private final String name;

    private final String iconLocation;

    private final Supplier<JPanel> panelSupplier;

    Game(String name, String iconLocation, Supplier<JPanel> panelSupplier) {
        this.name = name;
        this.iconLocation = iconLocation;
        this.panelSupplier = panelSupplier;
    }
}
