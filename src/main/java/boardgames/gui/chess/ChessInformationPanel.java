package boardgames.gui.chess;

import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ChessInformationPanel extends JPanel {

    private final JLabel turnIndicator;

    private final JLabel turnCounter;

    /**
     * Panel with informational labels and buttons.
     * @param chessPanel The root chess panel.
     */
    public ChessInformationPanel(ChessPanel chessPanel) {
        //Create panel layout
        setLayout(new GridLayout(1, 2));
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        JPanel buttonPanel = new JPanel();
        add(labelPanel);
        add(buttonPanel);

        //Add restart button
        JButton restartButton = new JButton("Restart");
        restartButton.addActionListener(e -> chessPanel.resetGame());
        buttonPanel.add(restartButton);

        //Add turn indicator
        JPanel turnIndicatorBox = new JPanel();
        turnIndicatorBox.setLayout(new GridLayout(1, 2));
        turnIndicatorBox.add(new JLabel("Player: "));
        turnIndicator = new JLabel("Yellow");
        turnIndicatorBox.add(turnIndicator);
        labelPanel.add(turnIndicatorBox);

        //Add turn counter
        JPanel turnCounterBox = new JPanel();
        turnCounterBox.setLayout(new GridLayout(1, 2));
        turnCounterBox.add(new JLabel("Turn: "));
        turnCounter = new JLabel("1");
        turnCounterBox.add(turnCounter);
        labelPanel.add(turnCounterBox);
    }

    /**
     * Called when the game progresses to next turn so labels can be updated.
     * @param isWhiteTurn Indicates if it is whites turn or not.
     */
    public void nextTurn(boolean isWhiteTurn) {
        turnIndicator.setText(isWhiteTurn ? "Yellow" : "Blue");
        if (isWhiteTurn) {
            turnCounter.setText((Integer.parseInt(turnCounter.getText()) + 1) + "");
        }
    }

    public void reset() {
        turnCounter.setText("1");
        turnIndicator.setText("Yellow");
    }
}
