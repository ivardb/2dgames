package boardgames.gui.chess;

import boardgames.logic.chess.ChessGame;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import lombok.Getter;

@Getter
public class ChessPanel extends JPanel {

    private ChessGame game;

    private final ChessBoardPanel boardPanel;

    private final ChessInformationPanel informationPanel;

    private final PieceMouseListener mouseListener;

    /**
     * Constructs the root chess panel with all sub panels.
     */
    public ChessPanel() {
        mouseListener = new PieceMouseListener(this);
        game = new ChessGame(this);
        boardPanel = new ChessBoardPanel(this, 80);
        boardPanel.setFocusable(true);
        boardPanel.requestFocusInWindow();
        informationPanel = new ChessInformationPanel(this);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(boardPanel);
        add(informationPanel);
    }

    /**
     * Resets to a new chess game.
     */
    public void resetGame() {
        game = new ChessGame(this);
        boardPanel.updateLabels();
        informationPanel.reset();
    }
}
