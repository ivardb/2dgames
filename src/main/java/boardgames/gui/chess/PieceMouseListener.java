package boardgames.gui.chess;

import static javax.swing.SwingUtilities.isLeftMouseButton;

import boardgames.gui.util.GridLabel;
import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputListener;
import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Setter
public class PieceMouseListener implements MouseInputListener {

    private ChessPanel chessPanel;

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (isLeftMouseButton(e)) {
            GridLabel label = (GridLabel) e.getSource();
            chessPanel.getGame().squareClicked(label.getRow(), label.getColumn());
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
