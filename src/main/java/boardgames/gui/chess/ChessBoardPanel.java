package boardgames.gui.chess;

import boardgames.gui.util.GridLabel;
import boardgames.logic.chess.board.ChessPosition;
import boardgames.logic.chess.board.pieces.Piece;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChessBoardPanel extends JPanel {

    private GridLabel[][] labels;

    private ChessPanel chessPanel;

    /**
     * Constructs a boardgames.logic.chess panel.
     * @param cellWidth width of grid cells.
     */
    @SuppressWarnings("SuspiciousNameCombination")
    public ChessBoardPanel(ChessPanel chessPanel, int cellWidth) {
        int rows = 8;
        int cols = 8;
        labels = new GridLabel[rows][cols];
        this.chessPanel = chessPanel;

        Dimension labelPrefSize = new Dimension(cellWidth, cellWidth);
        setLayout(new GridLayout(rows, cols));
        for (int row = 0; row < labels.length; row++) {
            for (int col = 0; col < labels[row].length; col++) {
                Color color = (col + row) % 2 == 0 ? Color.BLACK : Color.WHITE;
                GridLabel label = new GridLabel(row, col);
                label.addMouseListener(chessPanel.getMouseListener());
                label.setOpaque(true);
                label.setFont(new Font("Serif", Font.PLAIN, cellWidth));
                label.setBackground(color);
                label.setHorizontalAlignment(SwingConstants.CENTER);
                label.setVerticalAlignment(SwingConstants.CENTER);
                label.setPreferredSize(labelPrefSize);
                add(label);
                labels[row][col] = label;
            }
        }
        updateLabels();
    }

    /**
     * Updates the labels with the piece symbols.
     */
    public void updateLabels() {
        for (int row = 0; row < labels.length; row++) {
            for (int col = 0; col < labels[row].length; col++) {
                Piece piece = getPiece(row, col);
                JLabel label = getLabel(row, col);
                if (piece != null) {
                    if (piece.getColor() == 1) {
                        label.setForeground(Color.YELLOW);
                    } else {
                        label.setForeground(Color.BLUE);
                    }
                }
                String text = piece != null ? String.valueOf(piece.getSymbol()) : null;
                label.setText(text);
            }
        }
    }

    public Piece getPiece(int row, int col) {
        return chessPanel.getGame().getBoard().getPiece(row, col);
    }

    public JLabel getLabel(int row, int col) {
        return labels[row][col];
    }

    /**
     * Displays valid moves, green for moves, red for strikes.
     * @param moves List of moves to display.
     */
    public void displayValidMoves(List<ChessPosition> moves) {
        for (ChessPosition position : moves) {
            int row = position.getRow();
            int col = position.getColumn();
            Piece piece = getPiece(row, col);
            Color color = piece == null ? Color.GREEN : Color.RED;
            getLabel(row, col).setBackground(color);
        }
    }

    /**
     * Clears the valid moves being displayed.
     */
    public void clearValidMoves() {
        for (int row = 0; row < labels.length; row++) {
            for (int col = 0; col < labels[row].length; col++) {
                JLabel label = getLabel(row, col);
                Color color = (col + row) % 2 == 0 ? Color.BLACK : Color.WHITE;
                label.setBackground(color);
            }
        }
    }
}
