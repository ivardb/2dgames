package boardgames.gui;

import javax.swing.JMenu;
import javax.swing.JPanel;

public abstract class DynamicDropdownMenu extends JMenu {

    public DynamicDropdownMenu(String name) {
        super(name);
    }

    /**
     * Should update the menu to contain the correct items.
     * @param panel The active panel. Used to determine what should be active.
     * @return If the menu should be active or not.
     */
    public abstract boolean update(JPanel panel);
}
