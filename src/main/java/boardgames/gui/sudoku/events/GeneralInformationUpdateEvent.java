package boardgames.gui.sudoku.events;

import boardgames.gui.sudoku.panels.creation.GeneralPanel;
import java.util.EventObject;

public class GeneralInformationUpdateEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public GeneralInformationUpdateEvent(GeneralPanel source) {
        super(source);
    }

    @Override
    public GeneralPanel getSource() {
        return (GeneralPanel) super.getSource();
    }
}
