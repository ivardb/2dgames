package boardgames.gui.sudoku.events.listeners;

import boardgames.gui.sudoku.events.RuleSelectionChangeEvent;
import java.util.EventListener;

public interface RuleSelectionChangeListener extends EventListener {

    void onChange(RuleSelectionChangeEvent e);
}
