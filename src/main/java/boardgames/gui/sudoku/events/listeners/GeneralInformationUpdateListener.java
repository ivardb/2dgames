package boardgames.gui.sudoku.events.listeners;

import boardgames.gui.sudoku.events.GeneralInformationUpdateEvent;
import java.util.EventListener;

public interface GeneralInformationUpdateListener extends EventListener {

    void onUpdate(GeneralInformationUpdateEvent e);
}
