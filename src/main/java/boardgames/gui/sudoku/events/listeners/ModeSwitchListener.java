package boardgames.gui.sudoku.events.listeners;

import boardgames.gui.sudoku.events.ModeSwitchEvent;
import java.util.EventListener;

public interface ModeSwitchListener extends EventListener {

    void onSwitch(ModeSwitchEvent e);
}
