package boardgames.gui.sudoku.events.listeners;

import boardgames.gui.sudoku.events.BoxDefinitionUpdateEvent;
import java.util.EventListener;

public interface BoxDefinitionUpdateListener extends EventListener {

    void onUpdate(BoxDefinitionUpdateEvent e);
}
