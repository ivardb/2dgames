package boardgames.gui.sudoku.events;

import boardgames.logic.sudoku.SudokuGame;
import java.util.EventObject;
import lombok.Getter;

@Getter
public class ModeSwitchEvent extends EventObject {

    private final SudokuGame.Mode newMode;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public ModeSwitchEvent(Object source, SudokuGame.Mode newMode) {
        super(source);
        this.newMode = newMode;
    }
}
