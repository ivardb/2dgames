package boardgames.gui.sudoku.events;

import boardgames.gui.sudoku.panels.creation.RulePanel;
import java.util.EventObject;

public class RuleSelectionChangeEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public RuleSelectionChangeEvent(RulePanel source) {
        super(source);
    }

    @Override
    public RulePanel getSource() {
        return (RulePanel) super.getSource();
    }
}
