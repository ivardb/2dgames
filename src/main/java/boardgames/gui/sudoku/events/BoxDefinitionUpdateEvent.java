package boardgames.gui.sudoku.events;

import boardgames.gui.sudoku.panels.creation.BoxPanel;
import java.util.EventObject;

public class BoxDefinitionUpdateEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public BoxDefinitionUpdateEvent(BoxPanel source) {
        super(source);
    }

    @Override
    public BoxPanel getSource() {
        return (BoxPanel) super.getSource();
    }
}
