package boardgames.gui.sudoku.panels;

import boardgames.logic.sudoku.Rule;
import boardgames.logic.sudoku.SudokuPuzzle;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXLabel;

public class SudokuInformationPanel extends JPanel {

    JXLabel multiLineLabel;

    /**
     * Creates a panel displaying information from a puzzle.
     * @param puzzle The puzzle.
     */
    public SudokuInformationPanel(SudokuPuzzle puzzle) {
        multiLineLabel = new JXLabel();
        multiLineLabel.setMinimumSize(new Dimension(200, 0));
        multiLineLabel.setLineWrap(true);
        setText(puzzle);

        setPreferredSize(new Dimension(200, 350));
        add(multiLineLabel, BorderLayout.LINE_START);
    }

    /**
     * Sets the label text according to the given puzzle.
     * @param puzzle The puzzle to get the information from/
     */
    public void setText(SudokuPuzzle puzzle) {
        StringBuilder text = new StringBuilder("Name: " + puzzle.getName() + "\n\n");
        text.append("Rules: \n");
        for (Rule rule : puzzle.getRules()) {
            text.append("- ").append(rule.getName()).append("\n");
        }
        text.append("\nDescription: \n");
        text.append(puzzle.getDescription());
        multiLineLabel.setText(text.toString());
    }
}
