package boardgames.gui.sudoku.panels;

import boardgames.logic.sudoku.Sudoku;
import boardgames.logic.sudoku.SudokuCell;
import boardgames.logic.sudoku.SudokuChecker;
import boardgames.logic.sudoku.SudokuGame;
import boardgames.logic.sudoku.SudokuPuzzle;
import boardgames.logic.sudoku.util.Box;
import boardgames.logic.util.MoveDirection;
import boardgames.logic.util.Position;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import lombok.Getter;

@Getter
public class SudokuPanel extends JPanel {

    private final BoardPanel boardPanel;

    private final SudokuControlPanel controlPanel;

    private final SudokuGame game;

    /**
     * Creates a panel with default active modes.
     * @param puzzle The puzzle to display.
     */
    public SudokuPanel(SudokuPuzzle puzzle) {
        this(puzzle, Set.of(SudokuGame.Mode.FINAL,
                SudokuGame.Mode.CENTER_NOTE,
                SudokuGame.Mode.CORNER_NOTE,
                SudokuGame.Mode.COLORING));
    }

    /**
     * Constructs the root sudoku panel.
     */
    public SudokuPanel(SudokuPuzzle puzzle, Set<SudokuGame.Mode> activeModes) {
        Set<Box> boxes;
        if (puzzle == null) {
            puzzle = SudokuPuzzle.defaultEmptyClassicSudoku();
        }
        game = new SudokuGame(puzzle);
        boxes = puzzle.getBoxes();

        MouseListener mouseListener = game.getSelectionManager().getMouseListener();
        boardPanel = new BoardPanel(boxes, mouseListener,  60);
        controlPanel = new SudokuControlPanel(activeModes);
        controlPanel.addModeSwitchListener(e -> game.switchActiveMode(e.getNewMode()));

        boardPanel.update(game.getSudoku());

        game.getSudoku().addSudokuUpdateListener(e -> {
            Sudoku sudoku = e.getSource();
            boardPanel.update(sudoku);
            List<SudokuCell> mistakes = SudokuChecker.check(sudoku);
            List<Position> positions = mistakes.stream()
                    .map(SudokuCell::getPosition)
                    .collect(Collectors.toList());
            boardPanel.displayMistakes(positions);
        });
        game.getSelectionManager().addSelectionUpdateListener(e ->
                boardPanel.select(e.getSource().getSelected()));

        JPanel playWrapper = new JPanel();
        playWrapper.setLayout(new BoxLayout(playWrapper, BoxLayout.Y_AXIS));
        playWrapper.add(boardPanel);
        playWrapper.add(controlPanel);

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(playWrapper);
        add(new SudokuInformationPanel(puzzle));
        setFocusable(true);
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                char keyChar = e.getKeyChar();
                if (Character.isDigit(keyChar) && keyChar != '0') {
                    game.enterDigit(Character.getNumericValue(keyChar));
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        configureDefaultKeyBindings();
    }

    /**
     * Adds keybinding for various actions.
     */
    private void configureDefaultKeyBindings() {
        //Delete
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "delete");
        getActionMap().put("delete", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.delete();
            }
        });

        //region Movement
        //region Single selection
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "moveUp");
        getActionMap().put("moveUp", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.getSelectionManager().move(MoveDirection.UP);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "moveDown");
        getActionMap().put("moveDown", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.getSelectionManager().move(MoveDirection.DOWN);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "moveLeft");
        getActionMap().put("moveLeft", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.getSelectionManager().move(MoveDirection.LEFT);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "moveRight");
        getActionMap().put("moveRight", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.getSelectionManager().move(MoveDirection.RIGHT);
            }
        });
        //endregion
        //region expansion
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.SHIFT_DOWN_MASK), "addUp");
        getActionMap().put("addUp", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.getSelectionManager().expandSelection(MoveDirection.UP);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.SHIFT_DOWN_MASK),
                        "addDown");
        getActionMap()
                .put("addDown", new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        game.getSelectionManager().expandSelection(MoveDirection.DOWN);
                    }
                });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.SHIFT_DOWN_MASK),
                        "addLeft");
        getActionMap()
                .put("addLeft", new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        game.getSelectionManager().expandSelection(MoveDirection.LEFT);
                    }
                });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.SHIFT_DOWN_MASK),
                        "addRight");
        getActionMap()
                .put("addRight", new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        game.getSelectionManager().expandSelection(MoveDirection.RIGHT);
                    }
                });
        //endregion
        //endregion
    }
}
