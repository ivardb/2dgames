package boardgames.gui.sudoku.panels.creation;

import boardgames.gui.sudoku.events.GeneralInformationUpdateEvent;
import boardgames.gui.sudoku.events.listeners.GeneralInformationUpdateListener;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class GeneralPanel extends JPanel {

    private final Set<GeneralInformationUpdateListener> listeners;

    private final JTextField nameField;

    private final JTextArea description;

    /**
     * Creates a panel for changing generic information about the puzzle.
     */
    public GeneralPanel() {
        listeners = new HashSet<>();
        nameField = new JTextField(15);
        nameField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                notifyListeners();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                notifyListeners();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                notifyListeners();
            }
        });
        description = new JTextArea(5, 25);
        description.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                notifyListeners();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                notifyListeners();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                notifyListeners();
            }
        });

        setLayout(new FlowLayout());
        add(new JLabel("Name: "));
        add(nameField);
        add(new JLabel("Description:"));
        add(description);
        setPreferredSize(new Dimension(200, 500));
    }

    public String getName() {
        return nameField.getText();
    }

    public String getDescription() {
        return description.getText();
    }

    public void setTextFieldName(String name) {
        nameField.setText(name);
    }

    public void setDescription(String text) {
        description.setText(text);
    }

    public void addGeneralInformationUpdateListener(GeneralInformationUpdateListener listener) {
        listeners.add(listener);
    }

    private void notifyListeners() {
        for (GeneralInformationUpdateListener listener : listeners) {
            listener.onUpdate(new GeneralInformationUpdateEvent(this));
        }
    }
}
