package boardgames.gui.sudoku.panels.creation;

import boardgames.gui.sudoku.events.BoxDefinitionUpdateEvent;
import boardgames.gui.sudoku.events.listeners.BoxDefinitionUpdateListener;
import boardgames.gui.util.UnselectingJList;
import boardgames.logic.sudoku.SelectionManager;
import boardgames.logic.sudoku.util.Box;
import com.google.common.base.Joiner;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXLabel;

public class BoxPanel extends JPanel {

    private final Set<BoxDefinitionUpdateListener> listeners;

    private final List<Box> boxes;

    private Box selectedBox;

    /**
     * Creates a panel for updating the drawn boxes.
     * @param selectionManager The selection manager to get the current selection.
     * @param inputBoxes A set of initial boxes.
     */
    public BoxPanel(SelectionManager selectionManager, Set<Box> inputBoxes) {
        this.boxes = new ArrayList<>(inputBoxes);
        listeners = new HashSet<>();

        JList<String> boxList = new UnselectingJList<>(new AbstractListModel<>() {
            @Override
            public int getSize() {
                return boxes.size();
            }

            @Override
            public String getElementAt(int index) {
                return "Box: " + index;
            }
        });

        JXLabel boxDescription = new JXLabel();
        boxDescription.setLineWrap(true);

        JButton removeButton = new JButton("Remove box");
        removeButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (selectedBox != null) {
                    boxes.remove(selectedBox);
                    boxList.revalidate();
                    notifyListeners();
                    boxList.clearSelection();
                }
            }
        });

        JButton clearButton = new JButton("Clear boxes");
        clearButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boxes.clear();
                boxList.revalidate();
                boxDescription.setText("");
                notifyListeners();
            }
        });

        JButton addButton = new JButton("Add new box");
        addButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Box box = new Box(selectionManager.getSelected());
                selectionManager.clearSelection();
                boxes.add(box);
                boxList.revalidate();
                boxList.setSelectedIndex(boxes.size() - 1);
                notifyListeners();
            }
        });
        addButton.setFocusable(false);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setPreferredSize(new Dimension(250, 200));

        add(boxList);
        add(boxDescription);
        add(addButton);
        add(removeButton);
        add(clearButton);

        boxList.addListSelectionListener(e -> {
            if (boxList.isSelectionEmpty()) {
                boxDescription.setText("");
                selectedBox = null;
            } else {
                Box box = boxes.get(boxList.getSelectedIndex());
                List<String> strings = box.getPositions().stream()
                        .map(p -> "R" + (p.getRow() + 1 + "C" + (p.getCol() + 1)))
                        .collect(Collectors.toList());
                String text = "Positions: [" + Joiner.on(", ").join(strings) + "]";
                boxDescription.setText(text);
                selectedBox = box;
            }
        });
    }

    public void addBoxDefinitionUpdateListener(BoxDefinitionUpdateListener listener) {
        listeners.add(listener);
    }

    public Set<Box> getBoxes() {
        return new HashSet<>(boxes);
    }

    private void notifyListeners() {
        for (BoxDefinitionUpdateListener listener : listeners) {
            listener.onUpdate(new BoxDefinitionUpdateEvent(this));
        }
    }
}
