package boardgames.gui.sudoku.panels.creation;

import boardgames.Application;
import boardgames.gui.sudoku.panels.BoardPanel;
import boardgames.gui.sudoku.panels.SudokuControlPanel;
import boardgames.gui.sudoku.panels.SudokuSelectorPanel;
import boardgames.gui.sudoku.util.serialization.ImprovedObjectMapper;
import boardgames.gui.util.FileUtil;
import boardgames.logic.sudoku.Rule;
import boardgames.logic.sudoku.SelectionManager;
import boardgames.logic.sudoku.SudokuGame;
import boardgames.logic.sudoku.SudokuPuzzle;
import boardgames.logic.sudoku.util.Box;
import boardgames.logic.util.MoveDirection;
import boardgames.logic.util.Position;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class SudokuCreationPanel extends JPanel {

    private final BoardPanel boardPanel;

    private final SudokuPuzzle puzzle;

    private final SelectionManager selectionManager;

    private final JButton saveButton;

    private SudokuGame.Mode activeMode;

    public SudokuCreationPanel() {
        this(SudokuPuzzle.defaultEmptyClassicSudoku());
    }

    /**
     * Creates a panel used to edit and create sudokus.
     * @param sudokuPuzzle The sudoku to load.
     */
    public SudokuCreationPanel(SudokuPuzzle sudokuPuzzle) {
        //Create objects
        int cellSize = 60;
        puzzle = sudokuPuzzle;
        Set<Rule> rules = sudokuPuzzle.getRules();
        Set<Box> boxes = sudokuPuzzle.getBoxes();

        selectionManager = new SelectionManager();
        boardPanel = new BoardPanel(boxes, selectionManager.getMouseListener(), cellSize);
        final SudokuControlPanel sudokuControlPanel =
                new SudokuControlPanel(Set.of(SudokuGame.Mode.FINAL, SudokuGame.Mode.COLORING));
        final SudokuCreationControlPanel controlPanel =
                new SudokuCreationControlPanel(selectionManager, rules, boxes);
        controlPanel.getGeneralPanel().setTextFieldName(puzzle.getName());
        controlPanel.getGeneralPanel().setDescription(puzzle.getDescription());
        activeMode = SudokuGame.Mode.FINAL;

        //Configure layout.
        JPanel sudokuPanel = new JPanel();
        sudokuPanel.setLayout(new BoxLayout(sudokuPanel, BoxLayout.Y_AXIS));
        sudokuPanel.add(boardPanel);
        sudokuPanel.add(sudokuControlPanel);

        saveButton = new JButton("Save");
        saveButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
        saveButton.setEnabled(false);

        JPanel controlWrapper = new JPanel();
        controlWrapper.setLayout(new BoxLayout(controlWrapper, BoxLayout.Y_AXIS));
        controlWrapper.add(controlPanel);
        controlWrapper.add(saveButton);
        controlWrapper.add(javax.swing.Box.createVerticalGlue());

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(sudokuPanel);
        add(controlWrapper);

        //Add listeners to objects
        controlPanel.getRulePanel().addRuleSelectionChangeListener(e ->
                updateActiveRules(e.getSource().getSelectedRules()));
        controlPanel.getBoxPanel().addBoxDefinitionUpdateListener(e ->
                updateBoxes(e.getSource().getBoxes()));
        controlPanel.getGeneralPanel().addGeneralInformationUpdateListener(e ->
                updateGeneralInformation(e.getSource()));

        selectionManager.addSelectionUpdateListener(e ->
                boardPanel.select(e.getSource().getSelected()));
        sudokuControlPanel.addModeSwitchListener(e ->
                activeMode = e.getNewMode());
        puzzle.addPuzzleUpdateListener(e ->
                boardPanel.update(e.getSource()));
        boardPanel.addMouseListenerToLabels(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                getKeyboardFocus();
            }
        });

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (Character.isDigit(e.getKeyChar()) && e.getKeyChar() != '0') {
                    for (Position position : selectionManager.getSelected()) {
                        puzzle.enterDigit(position,
                                Character.getNumericValue(e.getKeyChar()),
                                activeMode);
                    }
                }
            }
        });

        boardPanel.update(puzzle);
        updateGeneralInformation(controlPanel.getGeneralPanel());
        addKeyBindings();

        addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                selectionManager.clearSelection();
            }
        });
    }

    private void updateActiveRules(Set<Rule> rules) {
        puzzle.setRules(rules);
    }

    private void updateBoxes(Set<Box> boxes) {
        puzzle.setBoxes(boxes);
    }

    private void updateGeneralInformation(GeneralPanel panel) {
        puzzle.setName(panel.getName());
        puzzle.setDescription(panel.getDescription());
        saveButton.setEnabled(puzzle.getName() != null && !puzzle.getName().isBlank());
    }

    private void getKeyboardFocus() {
        requestFocusInWindow();
    }

    private void save() {
        if (puzzle.getFilename() == null) {
            puzzle.setRandomFilename();
        }
        ObjectMapper objectMapper = new ImprovedObjectMapper();
        try {
            FileUtil.writeToFile(puzzle.getFilename(), objectMapper.writeValueAsString(puzzle));
            Application.goTo(new SudokuSelectorPanel());
        } catch (IOException e) {
            throw new RuntimeException("Something went wrong during saving", e);
        }
    }

    private void addKeyBindings() {
        //Delete
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "delete");
        getActionMap().put("delete", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectionManager.getSelected().forEach(puzzle::delete);
            }
        });

        //region Movement
        //region Single selection
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "moveUp");
        getActionMap().put("moveUp", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectionManager.move(MoveDirection.UP);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "moveDown");
        getActionMap().put("moveDown", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectionManager.move(MoveDirection.DOWN);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "moveLeft");
        getActionMap().put("moveLeft", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectionManager.move(MoveDirection.LEFT);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "moveRight");
        getActionMap().put("moveRight", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectionManager.move(MoveDirection.RIGHT);
            }
        });
        //endregion
        //region expansion
        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.SHIFT_DOWN_MASK), "addUp");
        getActionMap().put("addUp", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectionManager.expandSelection(MoveDirection.UP);
            }
        });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.SHIFT_DOWN_MASK),
                        "addDown");
        getActionMap()
                .put("addDown", new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        selectionManager.expandSelection(MoveDirection.DOWN);
                    }
                });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.SHIFT_DOWN_MASK),
                        "addLeft");
        getActionMap()
                .put("addLeft", new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        selectionManager.expandSelection(MoveDirection.LEFT);
                    }
                });

        getInputMap(WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.SHIFT_DOWN_MASK),
                        "addRight");
        getActionMap()
                .put("addRight", new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        selectionManager.expandSelection(MoveDirection.RIGHT);
                    }
                });
        //endregion
        //endregion
    }
}
