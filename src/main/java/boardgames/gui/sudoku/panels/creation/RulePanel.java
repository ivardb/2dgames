package boardgames.gui.sudoku.panels.creation;

import boardgames.gui.sudoku.events.RuleSelectionChangeEvent;
import boardgames.gui.sudoku.events.listeners.RuleSelectionChangeListener;
import boardgames.logic.sudoku.Rule;
import com.jidesoft.swing.CheckBoxList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.AbstractListModel;
import javax.swing.JPanel;
import javax.swing.ListModel;

public class RulePanel extends JPanel {

    private final Set<RuleSelectionChangeListener> listeners;

    private final CheckBoxList ruleList;

    /**
     * Creates a panel for editing the active sudoku rules.
     * @param rules The initial active rules.
     */
    public RulePanel(Set<Rule> rules) {
        listeners = new HashSet<>();
        ListModel<Rule> ruleModel = new AbstractListModel<>() {
            @Override
            public int getSize() {
                return Rule.values().length;
            }

            @Override
            public Rule getElementAt(int index) {
                return Rule.values()[index];
            }
        };
        ruleList = new CheckBoxList(ruleModel);
        ruleList.setCheckBoxListSelectedIndices(rules.stream().mapToInt(Rule::getIndex).toArray());
        add(ruleList);
        ruleList.getCheckBoxListSelectionModel().addListSelectionListener(e -> notifyListeners());
    }

    /**
     * Returns the currently selected rules.
     * @return A set of rules.
     */
    public Set<Rule> getSelectedRules() {
        return Arrays.stream(ruleList.getCheckBoxListSelectedValues())
                .map((o) -> (Rule) o)
                .collect(Collectors.toSet());
    }

    public void addRuleSelectionChangeListener(RuleSelectionChangeListener listener) {
        listeners.add(listener);
    }

    private void notifyListeners() {
        for (RuleSelectionChangeListener listener : listeners) {
            listener.onChange(new RuleSelectionChangeEvent(this));
        }
    }
}
