package boardgames.gui.sudoku.panels.creation;

import boardgames.logic.sudoku.Rule;
import boardgames.logic.sudoku.SelectionManager;
import boardgames.logic.sudoku.util.Box;
import java.util.Set;
import javax.swing.JTabbedPane;
import lombok.Getter;

@Getter
public class SudokuCreationControlPanel extends JTabbedPane {

    private final GeneralPanel generalPanel;

    private final RulePanel rulePanel;

    private final BoxPanel boxPanel;

    /**
     * Creates a panel for editing the sudoku.
     * @param selectionManager The selection manager, needed for editing boxes.
     * @param rules The default rules.
     * @param boxes The default boxes.
     */
    public SudokuCreationControlPanel(SelectionManager selectionManager,
                                      Set<Rule> rules,
                                      Set<Box> boxes) {
        //Create objects
        generalPanel = new GeneralPanel();
        rulePanel = new RulePanel(rules);
        boxPanel = new BoxPanel(selectionManager, boxes);

        //Add objects
        addTab("General", generalPanel);
        addTab("Rules", rulePanel);
        addTab("Boxes", boxPanel);
        setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
    }
}
