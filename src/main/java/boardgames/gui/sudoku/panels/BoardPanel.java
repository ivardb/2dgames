package boardgames.gui.sudoku.panels;

import boardgames.gui.sudoku.SudokuLabel;
import boardgames.gui.sudoku.util.BorderDrawer;
import boardgames.logic.sudoku.Sudoku;
import boardgames.logic.sudoku.SudokuCell;
import boardgames.logic.sudoku.SudokuPuzzle;
import boardgames.logic.sudoku.util.Box;
import boardgames.logic.util.Position;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JPanel;
import lombok.Getter;

@Getter
public class BoardPanel extends JPanel {

    private final SudokuLabel[][] labels;

    private final HashSet<SudokuLabel> selected;

    /**
     * Constructs the sudoku board panel.
     * @param mouseListener The listener for each of the labels.
     * @param cellSize The size of the cells.
     */
    public BoardPanel(Set<Box> boxes, MouseListener mouseListener, int cellSize) {
        int size = 9;
        labels = new SudokuLabel[size][size];

        setLayout(new GridLayout(size, size));
        setMinimumSize(new Dimension(size * cellSize, size * cellSize));
        setMaximumSize(new Dimension(size * cellSize, size * cellSize));

        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                SudokuLabel label = new SudokuLabel(new Position(row, col), cellSize);
                label.addMouseListener(mouseListener);
                add(label);
                labels[row][col] = label;
            }
        }
        drawBoxes(boxes);
        selected = new HashSet<>();
    }

    public void drawBoxes(Set<Box> boxes) {
        BorderDrawer.drawBoxes(boxes, labels);
    }

    /**
     * Selects the cells in the given positions.
     * @param positions The positions of the cells
     */
    public void select(Set<Position> positions) {
        // Check if there are invalid positions
        for (Position position : positions) {
            int row = position.getRow();
            int col = position.getCol();
            if (row < 0 || col < 0) {
                return;
            }
        }

        // Deselect current selected cells
        for (SudokuLabel sudokuLabel : selected) {
            sudokuLabel.deselect();
        }

        // Update the selected cells
        selected.clear();
        for (Position position : positions) {
            selected.add(labels[position.getRow()][position.getCol()]);
        }

        // Set the new background color for the selected cells.
        displaySelected();
    }

    /**
     * Sets the background color for all selected cells a little darker.
     */
    public void displaySelected() {
        for (SudokuLabel sudokuLabel : selected) {
            sudokuLabel.select();
        }
        highlight();
    }

    /**
     * Highlights the boxes that contain the same digit as the selected digit.
     */
    public void highlight() {
        for (SudokuLabel[] label : labels) {
            for (SudokuLabel sudokuLabel : label) {
                Integer digitToHighlight = null;
                if (selected.size() == 1) {
                    digitToHighlight = selected.iterator().next().getFinalDigit();
                }
                sudokuLabel.setHighlight(digitToHighlight);
            }
        }
    }

    /**
     * Displays the list of mistakes.
     * @param positions The positions that are wrong.
     */
    public void displayMistakes(List<Position> positions) {
        for (Position position : positions) {
            labels[position.getRow()][position.getCol()].setForeground(Color.RED);
        }
    }

    /**
     * Updates the grid given the sudoku.
     * Resets text color to default.
     * @param sudoku The sudoku
     */
    public void update(Sudoku sudoku) {
        for (int row = 0; row < labels.length; row++) {
            for (int col = 0; col < labels[0].length; col++) {
                SudokuLabel label = labels[row][col];
                SudokuCell cell = sudoku.getCell(new Position(row, col));
                if (cell.isFixed()) {
                    label.setForeground(Color.BLACK);
                } else {
                    label.setForeground(new Color(53, 83, 204));
                }
                label.setText(cell.getFinalDigit(), cell.getCornerNotes(), cell.getCenterNotes());
                setColor(row, col, sudoku.getCell(new Position(row, col)).getColor());
            }
        }
        displaySelected();
    }

    /**
     * Updates the digits and boxes given the puzzle.
     * @param puzzle The puzzle.
     */
    public void update(SudokuPuzzle puzzle) {
        drawBoxes(puzzle.getBoxes());
        for (int row = 0; row < labels.length; row++) {
            for (int col = 0; col < labels[0].length; col++) {
                SudokuLabel label = labels[row][col];
                label.setText(puzzle.getSudokuDigits()[row][col],
                        new ArrayList<>(),
                        new ArrayList<>());

                label.setBackground(puzzle.getColors()[row][col]);
            }
        }
        displaySelected();
    }


    /**
     * Sets the color for the given cell.
     * @param row   Row of the cell
     * @param col   Column of the cell
     * @param color Color of the cell
     */
    public void setColor(int row, int col, Color color) {
        SudokuLabel label = labels[row][col];
        label.setBackground(color);
    }

    /**
     * Adds additional mouse listeners to all of the labels.
     * @param mouseListener The mouse listener to add.
     */
    public void addMouseListenerToLabels(MouseListener mouseListener) {
        for (SudokuLabel[] label : labels) {
            for (SudokuLabel sudokuLabel : label) {
                sudokuLabel.addMouseListener(mouseListener);
            }
        }
    }
}
