package boardgames.gui.sudoku.panels;

import boardgames.gui.sudoku.events.ModeSwitchEvent;
import boardgames.gui.sudoku.events.listeners.ModeSwitchListener;
import boardgames.gui.util.FileUtil;
import boardgames.logic.sudoku.SudokuGame;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import lombok.Getter;

@Getter
public class SudokuControlPanel extends JPanel {

    private final Set<ModeSwitchListener> listeners;

    private final JLabel digit;

    private final JLabel corner;

    private final JLabel center;

    private final JLabel color;

    /**
     * Creates a sudoku control panel with all modes active.
     */
    public SudokuControlPanel() {
        this(Set.of(SudokuGame.Mode.FINAL,
                SudokuGame.Mode.CENTER_NOTE,
                SudokuGame.Mode.CORNER_NOTE,
                SudokuGame.Mode.COLORING));
    }

    /**
     * Create a panel to hold the controls and information for a sudoku game.
     */
    public SudokuControlPanel(Set<SudokuGame.Mode> activeModes) {
        listeners = new HashSet<>();

        digit = new JLabel(new ImageIcon(FileUtil.loadImage("sudoku/final.png")));
        corner = new JLabel(new ImageIcon(FileUtil.loadImage("sudoku/corner.png")));
        center = new JLabel(new ImageIcon(FileUtil.loadImage("sudoku/center.png")));
        color = new JLabel(new ImageIcon(FileUtil.loadImage("sudoku/color.png")));

        if (activeModes.contains(SudokuGame.Mode.FINAL)) {
            digit.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
            digit.setVerticalTextPosition(JLabel.BOTTOM);
            digit.setHorizontalTextPosition(JLabel.CENTER);
            digit.setText("A");
            digit.setBackground(Color.GREEN);
            digit.setOpaque(true);
            digit.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    switchActiveMode(SudokuGame.Mode.FINAL);
                }
            });
            add(digit);

            //Final digit mode
            getInputMap(WHEN_IN_FOCUSED_WINDOW)
                    .put(KeyStroke.getKeyStroke("A"), "switchToFinalDigit");
            getActionMap().put("switchToFinalDigit", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    switchActiveMode(SudokuGame.Mode.FINAL);
                }
            });
        }

        if (activeModes.contains(SudokuGame.Mode.CORNER_NOTE)) {
            corner.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
            corner.setVerticalTextPosition(JLabel.BOTTOM);
            corner.setHorizontalTextPosition(JLabel.CENTER);
            corner.setText("S");
            corner.setOpaque(true);
            corner.setBackground(Color.RED);
            corner.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    switchActiveMode(SudokuGame.Mode.CORNER_NOTE);
                }
            });
            add(corner);

            //Corner note mode
            getInputMap(WHEN_IN_FOCUSED_WINDOW)
                    .put(KeyStroke.getKeyStroke("S"), "switchToCornerNote");
            getActionMap().put("switchToCornerNote", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    switchActiveMode(SudokuGame.Mode.CORNER_NOTE);
                }
            });
        }

        if (activeModes.contains(SudokuGame.Mode.CENTER_NOTE)) {
            center.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
            center.setVerticalTextPosition(JLabel.BOTTOM);
            center.setHorizontalTextPosition(JLabel.CENTER);
            center.setText("D");
            center.setOpaque(true);
            center.setBackground(Color.RED);
            center.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    switchActiveMode(SudokuGame.Mode.CENTER_NOTE);
                }
            });
            add(center);

            //Center note mode
            getInputMap(WHEN_IN_FOCUSED_WINDOW)
                    .put(KeyStroke.getKeyStroke("D"), "switchToCenterNote");
            getActionMap().put("switchToCenterNote", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    switchActiveMode(SudokuGame.Mode.CENTER_NOTE);
                }
            });
        }

        if (activeModes.contains(SudokuGame.Mode.COLORING)) {
            color.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
            color.setVerticalTextPosition(JLabel.BOTTOM);
            color.setHorizontalTextPosition(JLabel.CENTER);
            color.setText("F");
            color.setOpaque(true);
            color.setBackground(Color.RED);
            color.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    switchActiveMode(SudokuGame.Mode.COLORING);
                }
            });
            add(color);

            //Coloring mode
            getInputMap(WHEN_IN_FOCUSED_WINDOW)
                    .put(KeyStroke.getKeyStroke("F"), "switchToColoring");
            getActionMap().put("switchToColoring", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    switchActiveMode(SudokuGame.Mode.COLORING);
                }
            });
        }
    }

    /**
     * Switch which mode is displayed as active.
     * @param mode The active mode.
     */
    private void switchActiveMode(SudokuGame.Mode mode) {
        if (mode == SudokuGame.Mode.FINAL) {
            digit.setBackground(Color.GREEN);
            corner.setBackground(Color.RED);
            center.setBackground(Color.RED);
            color.setBackground(Color.RED);
        } else if (mode == SudokuGame.Mode.CORNER_NOTE) {
            digit.setBackground(Color.RED);
            corner.setBackground(Color.GREEN);
            center.setBackground(Color.RED);
            color.setBackground(Color.RED);
        } else if (mode == SudokuGame.Mode.CENTER_NOTE) {
            digit.setBackground(Color.RED);
            corner.setBackground(Color.RED);
            center.setBackground(Color.GREEN);
            color.setBackground(Color.RED);
        } else if (mode == SudokuGame.Mode.COLORING) {
            digit.setBackground(Color.RED);
            corner.setBackground(Color.RED);
            center.setBackground(Color.RED);
            color.setBackground(Color.GREEN);
        }
        notifyListeners(mode);
    }

    public void addModeSwitchListener(ModeSwitchListener listener) {
        listeners.add(listener);
    }

    private void notifyListeners(SudokuGame.Mode newMode) {
        for (ModeSwitchListener listener : listeners) {
            listener.onSwitch(new ModeSwitchEvent(this, newMode));
        }
    }
}
