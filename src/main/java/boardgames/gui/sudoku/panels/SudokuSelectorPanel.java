package boardgames.gui.sudoku.panels;

import boardgames.Application;
import boardgames.gui.sudoku.panels.creation.SudokuCreationPanel;
import boardgames.gui.util.FileUtil;
import boardgames.gui.util.UnselectingJList;
import boardgames.logic.sudoku.SudokuPuzzle;
import boardgames.logic.sudoku.util.Box;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

public class SudokuSelectorPanel extends JPanel {

    private final List<SudokuPuzzle> puzzles;

    private SudokuPuzzle selected;

    private BoardPanel sudokuPreview;

    private SudokuInformationPanel informationPanel;

    /**
     * Creates a sudoku selection panel.
     */
    public SudokuSelectorPanel() {
        selected = SudokuPuzzle.defaultEmptyClassicSudoku();
        File puzzleFolder = new File("puzzles");
        if (!puzzleFolder.exists()) {
            boolean created = puzzleFolder.mkdir();
            if (!created) {
                throw new RuntimeException("Error creating puzzles folder!");
            }
        }
        List<File> puzzleFiles = List.of(puzzleFolder.listFiles());
        puzzles = puzzleFiles.stream()
                .map(SudokuPuzzle::fromFile)
                .collect(Collectors.toList());
        puzzles.sort(Comparator.comparing(SudokuPuzzle::getName));

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        //region listPanel
        ListModel<String> listModel = new AbstractListModel<>() {
            @Override
            public int getSize() {
                return puzzles.size();
            }

            @Override
            public String getElementAt(int index) {
                return puzzles.get(index).getName();
            }
        };
        JList<String> puzzleList = new UnselectingJList<>(listModel);
        puzzleList.addListSelectionListener(e -> {
            if (puzzleList.isSelectionEmpty()) {
                selected = SudokuPuzzle.defaultEmptyClassicSudoku();
            } else {
                selected = puzzles.get(puzzleList.getSelectedIndex());
            }
            sudokuPreview.update(selected);
            informationPanel.setText(selected);
        });

        JScrollPane listScroller = new JScrollPane(puzzleList);
        listScroller.setPreferredSize(new Dimension(150, 300));

        JButton playButton = new JButton("Play");
        playButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Application.goTo(new SudokuPanel(selected));
            }
        });

        JButton createButton = new JButton("New");
        createButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Application.goTo(new SudokuCreationPanel());
            }
        });

        JButton editButton = new JButton("Edit");
        editButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (selected != null) {
                    Application.goTo(new SudokuCreationPanel(selected));
                }
            }
        });

        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (selected != null) {
                    final SudokuPuzzle toBeDeleted = selected;
                    puzzles.remove(selected);
                    puzzleList.clearSelection();
                    puzzleList.revalidate();
                    repaint();
                    FileUtil.deleteFile(toBeDeleted.getFilename());
                }
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(2, 2));
        buttonPanel.add(playButton);
        buttonPanel.add(createButton);
        buttonPanel.add(editButton);
        buttonPanel.add(deleteButton);

        JPanel listPanel = new JPanel();
        listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.Y_AXIS));
        listPanel.add(listScroller);

        listPanel.add(buttonPanel);
        add(listPanel);
        //endregion

        sudokuPreview = new BoardPanel(Box.getDefaultSudokuBoxes(), null, 40);
        add(sudokuPreview);

        informationPanel = new SudokuInformationPanel(selected);
        add(informationPanel);
    }
}
