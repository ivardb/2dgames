package boardgames.gui.sudoku;

import boardgames.Application;
import boardgames.gui.DynamicDropdownMenu;
import boardgames.gui.sudoku.panels.SudokuPanel;
import boardgames.gui.sudoku.panels.SudokuSelectorPanel;
import boardgames.gui.sudoku.panels.creation.SudokuCreationPanel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class SudokuDropdownMenu extends DynamicDropdownMenu {

    public SudokuDropdownMenu() {
        super("Sudoku");
    }

    @Override
    public boolean update(JPanel panel) {
        if (panel instanceof SudokuPanel
                || panel instanceof SudokuSelectorPanel
                || panel instanceof SudokuCreationPanel) {
            removeAll();

            JMenuItem home = new JMenuItem("Exit puzzle");
            home.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Application.goTo(new SudokuSelectorPanel());
                }
            });
            add(home);
            return true;
        }
        return false;
    }
}
