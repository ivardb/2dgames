package boardgames.gui.sudoku;

import boardgames.logic.util.Position;
import com.google.common.base.Joiner;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import lombok.Getter;

public class SudokuLabel extends JLayeredPane {

    @Getter
    private final Position position;

    @Getter
    private Integer finalDigit;

    private Color backgroundColor;

    private final JLabel finalDigitLabel;

    private final JLabel cornerNotesLabel;

    private final JLabel centerNotesLabel;

    private final JLabel boxLabel;

    private final JLabel backgroundLabel;

    //Fonts
    private final Font digitFont;

    private final Font boldDigitFont;

    private final Font notesFont;

    private final Font boldNotesFont;

    public SudokuLabel(Position position, int cellSize) {
        this(position, cellSize, true);
    }

    /**
     * Constructs a multilayer sudoku label.
     * @param position The position in the grid.
     * @param cellSize The size of the cell.
     */
    public SudokuLabel(Position position, int cellSize, boolean withGrid) {
        super();
        this.position = position;
        backgroundColor = Color.WHITE;

        //Create fonts
        digitFont = new Font(Font.DIALOG, Font.PLAIN, cellSize);
        boldDigitFont = new Font(Font.DIALOG, Font.BOLD, cellSize + 3);
        notesFont = new Font(Font.DIALOG, Font.PLAIN, 15);
        boldNotesFont = new Font(Font.DIALOG, Font.BOLD, 18);

        setOpaque(true);
        //Create labels.
        Dimension dimension = new Dimension(cellSize, cellSize);
        setPreferredSize(dimension);
        finalDigitLabel = new JLabel();
        finalDigitLabel.setFont(digitFont);
        finalDigitLabel.setHorizontalAlignment(SwingConstants.CENTER);
        finalDigitLabel.setVerticalAlignment(SwingConstants.CENTER);
        finalDigitLabel.setPreferredSize(dimension);
        finalDigitLabel.setOpaque(false);
        finalDigitLabel.setBounds(0, 0, cellSize, cellSize);

        cornerNotesLabel = new JLabel();
        cornerNotesLabel.setFont(notesFont);
        cornerNotesLabel.setHorizontalAlignment(SwingConstants.LEFT);
        cornerNotesLabel.setVerticalAlignment(SwingConstants.TOP);
        cornerNotesLabel.setPreferredSize(dimension);
        cornerNotesLabel.setOpaque(false);
        cornerNotesLabel.setBounds(0, 0, cellSize, cellSize);

        centerNotesLabel = new JLabel();
        centerNotesLabel.setFont(notesFont);
        centerNotesLabel.setHorizontalAlignment(SwingConstants.CENTER);
        centerNotesLabel.setVerticalAlignment(SwingConstants.CENTER);
        centerNotesLabel.setPreferredSize(dimension);
        centerNotesLabel.setOpaque(false);
        centerNotesLabel.setBounds(0, 0, cellSize, cellSize);

        boxLabel = new JLabel();
        boxLabel.setBounds(0, 0, cellSize, cellSize);

        backgroundLabel = new JLabel();
        backgroundLabel.setOpaque(true);
        backgroundLabel.setBackground(Color.WHITE);
        backgroundLabel.setBounds(0, 0, cellSize, cellSize);

        add(finalDigitLabel);
        add(cornerNotesLabel);
        add(centerNotesLabel);
        add(boxLabel);
        add(backgroundLabel);
        setLayer(finalDigitLabel, 102);
        setLayer(cornerNotesLabel, 100);
        setLayer(centerNotesLabel, 101);
        setLayer(boxLabel, 10);
        setLayer(backgroundLabel, 1);

        if (withGrid) {
            drawGridBorder();
        }
    }

    @Override
    public void setBackground(Color color) {
        backgroundColor = color;
        backgroundLabel.setBackground(color);
        repaint();
    }

    @Override
    public void setForeground(Color color) {
        finalDigitLabel.setForeground(color);
        centerNotesLabel.setForeground(color);
        cornerNotesLabel.setForeground(color);
        repaint();
    }

    @Override
    public void setBorder(Border border) {

    }

    public void drawBoxBorder(Border border) {
        boxLabel.setBorder(border);
    }

    public void select() {
        backgroundLabel.setBackground(backgroundColor.darker());
        repaint();
    }

    public void deselect() {
        backgroundLabel.setBackground(backgroundColor);
        repaint();
    }

    /**
     * Highlights the text if it contains the given digit.
     * @param digit The digit to check for highlighting.
     */
    public void setHighlight(Integer digit) {
        boolean highlight;
        if (digit == null) {
            highlight = false;
        } else {
            highlight = digit.equals(finalDigit)
                    || cornerNotesLabel.getText().contains(digit + "")
                    || centerNotesLabel.getText().contains(digit + "");
        }
        if (highlight) {
            finalDigitLabel.setFont(boldDigitFont);
            centerNotesLabel.setFont(boldNotesFont);
            cornerNotesLabel.setFont(boldNotesFont);
        } else {
            finalDigitLabel.setFont(digitFont);
            centerNotesLabel.setFont(notesFont);
            cornerNotesLabel.setFont(notesFont);
        }
    }

    /**
     * Sets the correct text for the given digits.
     * @param finalDigit The large final digit, if not null will be the only visible digit.
     * @param cornerNotes List of notes in the top left corner, only visible if finalDigit is null.
     * @param centerNotes List of notes in the center, only visible if finalDigit is null.
     */
    public void setText(Integer finalDigit, List<Integer> cornerNotes, List<Integer> centerNotes) {
        if (finalDigit == null) {
            finalDigitLabel.setText("");
            cornerNotesLabel.setText(" " + Joiner.on(" ").join(cornerNotes));
            centerNotesLabel.setText(Joiner.on("").join(centerNotes));
        } else {
            finalDigitLabel.setText(finalDigit + "");
            cornerNotesLabel.setText("");
            centerNotesLabel.setText("");
        }
        this.finalDigit = finalDigit;
        repaint();
    }

    private void drawGridBorder() {
        int left = position.getCol() == 0 ? 1 : 0;
        int top = position.getRow() == 0 ? 1 : 0;
        backgroundLabel.setBorder(BorderFactory.createMatteBorder(top, left, 1, 1, Color.BLACK));
    }
}
