package boardgames.gui.sudoku.util;

import boardgames.gui.sudoku.SudokuLabel;
import boardgames.logic.sudoku.util.Box;
import boardgames.logic.util.MoveDirection;
import boardgames.logic.util.Position;
import java.awt.Color;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.BorderFactory;

public class BorderDrawer {

    /**
     * Draws additional thick borders around the given boxes.
     * @param boxes The boxes to draw.
     * @param labels The labels of the sudoku in which to draw the boxes.
     */
    public static void drawBoxes(Set<Box> boxes, SudokuLabel[][] labels) {
        for (int row = 0; row < labels.length; row++) {
            for (int col = 0; col < labels[row].length; col++) {
                Position position = new Position(row, col);
                List<Box> inBoxes = inBoxes(position, boxes);
                if (inBoxes.isEmpty()) {
                    int left = 0;
                    int right = 0;
                    int top = 0;
                    int bottom = 0;
                    if (inBoxes(position.move(MoveDirection.DOWN), boxes).size() > 0) {
                        bottom = 4;
                    }
                    if (inBoxes(position.move(MoveDirection.RIGHT), boxes).size() > 0) {
                        right = 4;
                    }
                    labels[row][col].drawBoxBorder(
                            BorderFactory.createMatteBorder(top, left, bottom, right, Color.BLACK));
                } else if (inBoxes.size() == 1) {
                    Box box = inBoxes.get(0);
                    int left = 0;
                    int right = 0;
                    int top = 0;
                    int bottom = 0;
                    if (row == 0) {
                        top = 4;
                    }
                    if (col == 0) {
                        left = 4;
                    }
                    if (!box.getPositions().contains(position.move(MoveDirection.DOWN))) {
                        bottom = 4;
                    }
                    if (!box.getPositions().contains(position.move(MoveDirection.RIGHT))) {
                        right = 4;
                    }
                    labels[row][col].drawBoxBorder(
                            BorderFactory.createMatteBorder(top, left, bottom, right, Color.BLACK));
                } else {
                    throw new RuntimeException("Overlapping boxes");
                }
            }
        }
    }

    private static List<Box> inBoxes(Position position, Set<Box> boxes) {
        return boxes.stream()
                .filter(box -> box.getPositions().contains(position))
                .collect(Collectors.toList());
    }
}
