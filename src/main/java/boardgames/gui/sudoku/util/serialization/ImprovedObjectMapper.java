package boardgames.gui.sudoku.util.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.awt.Color;

public class ImprovedObjectMapper extends ObjectMapper {

    /**
     * Creates an object mapper that can handle additional classes.
     */
    public ImprovedObjectMapper() {
        super();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Color.class, new ColorSerializer());
        module.addDeserializer(Color.class, new ColorDeserializer());
        super.registerModule(module);
    }
}
