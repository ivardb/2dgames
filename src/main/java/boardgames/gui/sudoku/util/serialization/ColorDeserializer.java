package boardgames.gui.sudoku.util.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.awt.Color;
import java.io.IOException;

public class ColorDeserializer extends JsonDeserializer<Color> {

    @Override
    public Color deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode root = p.getCodec().readTree(p);
        TextNode rgba = (TextNode) root.get("argb");
        if (rgba != null) {
            return new Color(Integer.parseUnsignedInt(rgba.textValue(), 16), true);
        } else {
            return Color.WHITE;
        }
    }
}
