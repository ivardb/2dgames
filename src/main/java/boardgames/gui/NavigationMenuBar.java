package boardgames.gui;

import boardgames.Application;
import boardgames.gui.sudoku.SudokuDropdownMenu;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class NavigationMenuBar extends JMenuBar {

    private List<DynamicDropdownMenu> activeMenus;

    private List<DynamicDropdownMenu> deActiveMenus;

    /**
     * Constructs the navigation menu.
     */
    public NavigationMenuBar() {
        DynamicDropdownMenu home = new DynamicDropdownMenu("Application") {
            @Override
            public boolean update(JPanel panel) {
                return true;
            }
        };
        JMenuItem mainMenu = new JMenuItem("Main menu");
        mainMenu.addActionListener(e -> Application.goTo(new GameMenuPanel()));
        JMenuItem exitProgram = new JMenuItem("Exit program");
        exitProgram.addActionListener(e -> Application.exit());
        home.add(mainMenu);
        home.add(exitProgram);
        add(home);
        activeMenus = new ArrayList<>();
        deActiveMenus = new ArrayList<>();
        activeMenus.add(home);
        deActiveMenus.add(new SudokuDropdownMenu());
    }

    /**
     * Update according to active panel.
     * @param activePanel The currently active panel.
     */
    public void update(JPanel activePanel) {
        List<DynamicDropdownMenu> active = new ArrayList<>();
        List<DynamicDropdownMenu> deActive = new ArrayList<>();
        for (DynamicDropdownMenu menu : activeMenus) {
            if (menu.update(activePanel)) {
                active.add(menu);
            } else {
                deActive.add(menu);
            }
        }
        for (DynamicDropdownMenu menu : deActiveMenus) {
            if (menu.update(activePanel)) {
                active.add(menu);
            } else {
                deActive.add(menu);
            }
        }
        activeMenus = active;
        deActiveMenus = deActive;
        updateActive();
    }

    private void updateActive() {
        removeAll();
        for (DynamicDropdownMenu menu : activeMenus) {
            add(menu);
        }
    }
}
