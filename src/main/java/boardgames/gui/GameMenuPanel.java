package boardgames.gui;

import static javax.swing.SwingUtilities.isLeftMouseButton;

import boardgames.Application;
import boardgames.Game;
import boardgames.gui.util.FileUtil;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

public class GameMenuPanel extends JPanel {

    /**
     * Constructs the game menu panel with all game images.
     */
    public GameMenuPanel() {
        setLayout(new FlowLayout(FlowLayout.LEADING));
        setPreferredSize(new Dimension(800, 500));
        for (Game game : Game.values()) {
            BufferedImage image = FileUtil.loadImage(game.getIconLocation());
            JLabel label = new JLabel(new ImageIcon(image));
            label.addMouseListener(new MouseInputListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (isLeftMouseButton(e)) {
                        Application.goTo(game.getPanelSupplier().get());
                    }
                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }

                @Override
                public void mouseDragged(MouseEvent e) {

                }

                @Override
                public void mouseMoved(MouseEvent e) {

                }
            });
            add(label);
        }

    }
}
