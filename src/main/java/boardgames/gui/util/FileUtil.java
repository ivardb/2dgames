package boardgames.gui.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

public class FileUtil {

    /**
     * Loads the image resource as BufferedImage.
     * @param resource The path from the resource folder.
     * @return The image as BufferedImage.
     */
    public static BufferedImage loadImage(String resource) {
        try {
            InputStream inputStream = FileUtil.class.getClassLoader().getResourceAsStream(resource);
            if (inputStream != null) {
                return ImageIO.read(inputStream);
            } else {
                throw new RuntimeException();
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /**
     * Loads a generic file, not in the resource folder.
     * @param path The path to the file.
     * @return An InputStream.
     */
    public static InputStream loadFile(String path) {
        try {
            return new FileInputStream(new File(path));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Loads a generic file, not in the resource folder.
     * @param file The path to the file as File.
     * @return An InputStream.
     */
    public static InputStream loadFile(File file) {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the data to the filename. Does not append.
     * @param filename The filename of the file to write to, not in the resource folder.
     * @param data The data to write.
     */
    public static void writeToFile(String filename, String data) {
        File file = new File(filename);
        try {
            FileWriter fileWriter = new FileWriter(file, false);
            fileWriter.write(data);
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void deleteFile(String filename) {
        deleteFile(new File(filename));
    }

    public static void deleteFile(File file) {
        file.delete();
    }
}
