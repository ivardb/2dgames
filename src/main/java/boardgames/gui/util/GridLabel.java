package boardgames.gui.util;

import javax.swing.JLabel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class GridLabel extends JLabel {

    private int row;

    private int column;

    @Override
    public int hashCode() {
        return 100 * row + column;
    }
}
