package boardgames;

import boardgames.gui.GameMenuPanel;
import boardgames.gui.NavigationMenuBar;
import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Application {

    private static JFrame frame;

    private static JPanel centerPanel;

    private static NavigationMenuBar menuBar;

    /**
     * Main application class.
     * @param args args.
     */
    public static void main(String[] args) {
        frame = new JFrame("The Boris Bear, Blitzing Board Games");

        menuBar = new NavigationMenuBar();
        frame.setJMenuBar(menuBar);
        centerPanel = new GameMenuPanel();
        frame.getContentPane().add(centerPanel, BorderLayout.CENTER);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Switches the main panel to the new panel.
     * Can be used to navigate the application.
     * @param jpanel The new central JPanel
     */
    public static void goTo(JPanel jpanel) {
        frame.getContentPane().remove(centerPanel);
        centerPanel = jpanel;
        frame.getContentPane().add(jpanel, BorderLayout.CENTER);
        jpanel.requestFocusInWindow();
        menuBar.update(jpanel);
        frame.revalidate();
        frame.repaint();
        frame.pack();
    }

    public static void exit() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
