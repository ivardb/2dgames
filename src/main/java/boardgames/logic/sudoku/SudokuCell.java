package boardgames.logic.sudoku;

import boardgames.logic.util.Position;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SudokuCell {

    private final Position position;

    private List<Integer> centerNotes;

    private List<Integer> cornerNotes;

    private Integer finalDigit;

    private final boolean fixed;

    private Color color;

    /**
     * Constructs a sudoku cell.
     * @param position the position.
     */
    public SudokuCell(Position position) {
        this.position = position;
        centerNotes = new ArrayList<>();
        cornerNotes = new ArrayList<>();
        fixed = false;
        this.color = Color.WHITE;
    }

    /**
     * Used to give starting value.
     * @param position The position.
     * @param digit The given digit.
     */
    public SudokuCell(Position position, int digit, Color color) {
        this.position = position;
        centerNotes = new ArrayList<>();
        cornerNotes = new ArrayList<>();
        fixed = true;
        finalDigit = digit;
        this.color = color;
    }

    /**
     * Enters a final (large) digit.
     * @param digit     Digit to enter
     */
    public void enterFinalDigit(int digit) {
        if (fixed) {
            return;
        }
        finalDigit = digit;
    }


    /**
     * Adds or removes a corner note.
     * @param digit The note digit.
     */
    public void enterCornerNote(int digit) {
        if (cornerNotes.contains(digit)) {
            cornerNotes.remove((Object) digit);
        } else {
            cornerNotes.add(digit);
        }
        cornerNotes.sort(Comparator.comparing(Function.identity()));
    }

    /**
     * Adds or removes a center note.
     * @param digit The note digit.
     */
    public void enterCenterNote(int digit) {
        if (centerNotes.contains(digit)) {
            centerNotes.remove((Object) digit);
        } else {
            centerNotes.add(digit);
        }
        centerNotes.sort(Comparator.comparing(Function.identity()));
    }

    /**
     * Deletes either the final digit or the notes.
     */
    public void delete(SudokuGame.Mode mode) {
        if (fixed) {
            return;
        }
        if (finalDigit == null) {
            if (mode.equals(SudokuGame.Mode.COLORING)) {
                setColor(Color.WHITE);
            } else if (mode.equals(SudokuGame.Mode.CENTER_NOTE)) {
                centerNotes = new ArrayList<>();
            } else if (mode.equals(SudokuGame.Mode.CORNER_NOTE)) {
                cornerNotes = new ArrayList<>();
            }
        } else {
            finalDigit = null;
        }
    }

    /**
     * Sets the color of the SudokuCell.
     * @param color Color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * Returns a copy of the SudokuCell.
     * @return SudokuCell object
     */
    public SudokuCell copy() {
        Position position = new Position(this.position.getRow(), this.position.getCol());
        List<Integer> centerNotes = new ArrayList<>(this.centerNotes);
        List<Integer> cornerNotes = new ArrayList<>(this.cornerNotes);
        return new SudokuCell(position, centerNotes, cornerNotes, finalDigit, fixed, color);
    }
}
