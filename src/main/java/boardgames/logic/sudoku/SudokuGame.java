package boardgames.logic.sudoku;

import boardgames.logic.util.Position;
import java.awt.Color;
import lombok.Getter;

@Getter
public class SudokuGame {

    private final Sudoku sudoku;

    private Mode activeMode;

    private final SelectionManager selectionManager;

    /**
     * Constructs a sudoku from the given puzzle.
     * @param puzzle The puzzle.
     */
    public SudokuGame(SudokuPuzzle puzzle) {
        activeMode = Mode.FINAL;
        sudoku = new Sudoku(puzzle);
        selectionManager = new SelectionManager();
    }

    /**
     * Creates a sudoku game.
     */
    public SudokuGame() {
        activeMode = Mode.FINAL;
        sudoku = new Sudoku(SudokuPuzzle.defaultEmptyClassicSudoku());
        selectionManager = new SelectionManager();
    }

    /**
     * Enters a digit using the active mode.
     * @param digit The digit.
     */
    public void enterDigit(int digit) {
        if (activeMode != Mode.COLORING) {
            for (Position position : selectionManager.getSelected()) {
                sudoku.enterDigit(position, digit, activeMode);
            }
        } else {
            color(digit);
        }
    }

    /**
     * Changes the active mode.
     * @param newMode The new active mode.
     */
    public void switchActiveMode(Mode newMode) {
        activeMode = newMode;
    }

    /**
     * Delete action on the selected square.
     */
    public void delete() {
        for (Position position : selectionManager.getSelected()) {
            sudoku.delete(position, activeMode);
        }
    }

    /**
     * Color the selection with the given color number.
     * @param digit The number of the color to use for coloring.
     */
    public void color(int digit) {
        Color color;
        switch (digit) {
            case 2:
                color = new Color(41, 156, 71);
                break;
            case 3:
                color = new Color(56, 176, 209);
                break;
            case 4:
                color = new Color(212, 212, 55);
                break;
            case 5:
                color = new Color(222, 149, 24);
                break;
            case 6:
                color = new Color(255, 175, 175);
                break;
            case 7:
                color = new Color(186, 21, 191);
                break;
            case 8:
                color = new Color(128, 128, 128);
                break;
            case 9:
                color = new Color(0, 0, 0);
                break;
            default:
                color = new Color(255, 255, 255);
        }

        for (Position position : selectionManager.getSelected()) {
            sudoku.color(position, color);
        }
    }

    public enum Mode {
        FINAL,
        CENTER_NOTE,
        CORNER_NOTE,
        COLORING
    }
}
