package boardgames.logic.sudoku.events.listeners;

import boardgames.logic.sudoku.events.SudokuUpdateEvent;
import java.util.EventListener;

public interface SudokuUpdateListener extends EventListener {

    void onUpdate(SudokuUpdateEvent e);
}
