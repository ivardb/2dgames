package boardgames.logic.sudoku.events.listeners;

import boardgames.logic.sudoku.events.SelectionUpdateEvent;
import java.util.EventListener;

public interface SelectionUpdateListener extends EventListener {

    void onUpdate(SelectionUpdateEvent e);
}
