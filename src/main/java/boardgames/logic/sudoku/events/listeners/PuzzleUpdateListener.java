package boardgames.logic.sudoku.events.listeners;

import boardgames.logic.sudoku.events.PuzzleUpdateEvent;
import java.util.EventListener;

public interface PuzzleUpdateListener extends EventListener {

    void onUpdate(PuzzleUpdateEvent e);
}
