package boardgames.logic.sudoku.events;

import boardgames.logic.sudoku.SelectionManager;
import java.util.EventObject;

public class SelectionUpdateEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the SelectionManager on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public SelectionUpdateEvent(SelectionManager source) {
        super(source);
    }

    @Override
    public SelectionManager getSource() {
        return (SelectionManager) super.getSource();
    }
}
