package boardgames.logic.sudoku.events;

import boardgames.logic.sudoku.SudokuPuzzle;
import java.util.EventObject;

public class PuzzleUpdateEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public PuzzleUpdateEvent(SudokuPuzzle source) {
        super(source);
    }

    @Override
    public SudokuPuzzle getSource() {
        return (SudokuPuzzle) super.getSource();
    }
}
