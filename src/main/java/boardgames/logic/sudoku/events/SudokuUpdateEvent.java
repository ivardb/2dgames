package boardgames.logic.sudoku.events;

import boardgames.logic.sudoku.Sudoku;
import java.util.EventObject;

public class SudokuUpdateEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source the Sudoku on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public SudokuUpdateEvent(Sudoku source) {
        super(source);
    }

    @Override
    public Sudoku getSource() {
        return (Sudoku) super.getSource();
    }
}
