package boardgames.logic.sudoku;

import boardgames.logic.sudoku.events.SudokuUpdateEvent;
import boardgames.logic.sudoku.events.listeners.SudokuUpdateListener;
import boardgames.logic.sudoku.util.Box;
import boardgames.logic.util.Position;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;

public class Sudoku {

    private final Set<SudokuUpdateListener> listeners;

    private final SudokuCell[][] sudokuCells;

    @Getter
    private final Set<Box> boxes;

    @Getter
    private final Set<Rule> rules;

    /**
     * Creates a sudoku with the given values already entered.
     * @param puzzle The input puzzle.
     */
    public Sudoku(SudokuPuzzle puzzle) {
        boxes = puzzle.getBoxes();
        rules = puzzle.getRules();
        listeners = new HashSet<>();
        Integer[][] inputPuzzle = puzzle.getSudokuDigits();
        sudokuCells = new SudokuCell[inputPuzzle.length][inputPuzzle.length];
        for (int row = 0; row < inputPuzzle.length; row++) {
            for (int col = 0; col < inputPuzzle.length; col++) {
                Position position = new Position(row, col);
                if (inputPuzzle[row][col] == null) {
                    sudokuCells[row][col] = new SudokuCell(position);
                } else {
                    sudokuCells[row][col] = new SudokuCell(position, inputPuzzle[row][col],
                            Color.WHITE);
                }
                sudokuCells[row][col].setColor(puzzle.getColors()[row][col]);
            }
        }
    }

    //region data retrieval
    public SudokuCell getCell(Position position) {
        return sudokuCells[position.getRow()][position.getCol()].copy();
    }

    public List<SudokuCell> getRow(int row) {
        return copyCells(new ArrayList<>(Arrays.asList(sudokuCells[row])));
    }

    /**
     * Gets a list of all of the rows of the sudoku.
     * @return A list of a list of sudoku cells for each of the rows.
     */
    public List<List<SudokuCell>> getRows() {
        List<List<SudokuCell>> rows = new ArrayList<>();
        for (int row = 0; row < sudokuCells.length; row++) {
            rows.add(getRow(row));
        }
        return rows;
    }

    /**
     * Gets the requested column.
     * @param col The column number zero-indexed.
     * @return A list of the cells belonging to the column.
     */
    public List<SudokuCell> getColumn(int col) {
        List<SudokuCell> result = new ArrayList<>();
        for (SudokuCell[] sudokuCell : sudokuCells) {
            result.add(sudokuCell[col]);
        }
        return copyCells(result);
    }

    /**
     * Gets a list of all the column values.
     * @return A list of columns.
     */
    public List<List<SudokuCell>> getColumns() {
        List<List<SudokuCell>> columns = new ArrayList<>();
        for (int col = 0; col < sudokuCells.length; col++) {
            columns.add(getColumn(col));
        }
        return columns;
    }

    /**
     * Gets the two diagonals in a list.
     * @return A list of a list for each of the diagonals.
     */
    public List<List<SudokuCell>> getDiagonals() {
        List<SudokuCell> diagonal = new ArrayList<>();
        List<SudokuCell> diagonal2 = new ArrayList<>();
        for (int i = 0; i < sudokuCells.length; i++) {
            diagonal.add(sudokuCells[i][i]);
            diagonal2.add(sudokuCells[i][sudokuCells.length - 1 - i]);
        }
        return Arrays.asList(copyCells(diagonal), copyCells(diagonal2));
    }
    //endregion

    /**
     * Creates an Integer array for each of the final digits already entered.
     * @return Integer array.
     */
    public Integer[][] toIntegerArray() {
        Integer[][] res = new Integer[sudokuCells.length][sudokuCells.length];
        for (int row = 0; row < sudokuCells.length; row++) {
            for (int col = 0; col < sudokuCells.length; col++) {
                res[row][col] = sudokuCells[row][col].getFinalDigit();
            }
        }
        return res;
    }

    //region data updating

    /**
     * Enters the digit in the given mode.
     * @param position The position to enter it in.
     * @param digit The digit to enter.
     * @param mode The mode to use.
     */
    public void enterDigit(Position position, int digit, SudokuGame.Mode mode) {
        SudokuCell sudokuCell = getOriginalCell(position);
        if (mode.equals(SudokuGame.Mode.FINAL)) {
            sudokuCell.enterFinalDigit(digit);
        } else if (mode.equals(SudokuGame.Mode.CORNER_NOTE)) {
            sudokuCell.enterCornerNote(digit);
        } else if (mode.equals(SudokuGame.Mode.CENTER_NOTE)) {
            sudokuCell.enterCenterNote(digit);
        }
        notifyListeners();
    }

    /**
     * Colors the position with the given color.
     * @param position The position to color.
     * @param color The color.
     */
    public void color(Position position, Color color) {
        getOriginalCell(position).setColor(color);
        notifyListeners();
    }

    /**
     * Deletes the digits at the given position.
     * @param position The position to delete from.
     */
    public void delete(Position position, SudokuGame.Mode mode) {
        getOriginalCell(position).delete(mode);
        notifyListeners();
    }

    private SudokuCell getOriginalCell(Position position) {
        return sudokuCells[position.getRow()][position.getCol()];
    }

    //endregion

    /**
     * Adds a sudoku update listener.
     * @param listener The listener to add.
     */
    public void addSudokuUpdateListener(SudokuUpdateListener listener) {
        listeners.add(listener);
    }

    /**
     * Notify all registered listeners that the sudoku has been updated.
     */
    private void notifyListeners() {
        for (SudokuUpdateListener listener : listeners) {
            listener.onUpdate(new SudokuUpdateEvent(this));
        }
    }

    /**
     * Creates a copy of each cell in the list.
     * @param sudokuCells   List of SudokuCells
     * @return              Copied list of SudokuCells
     */
    private List<SudokuCell> copyCells(List<SudokuCell> sudokuCells) {
        return sudokuCells.stream().map(SudokuCell::copy).collect(Collectors.toList());
    }
}
