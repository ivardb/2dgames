package boardgames.logic.sudoku;

import java.util.ArrayList;
import java.util.List;

public class SudokuChecker {

    /**
     * Checks the sudoku for the provided rules.
     * @param sudoku The sudoku to check.
     * @return Cells that cause a conflict.
     */
    public static List<SudokuCell> check(Sudoku sudoku) {
        List<SudokuCell> incorrect = new ArrayList<>();
        for (Rule rule : sudoku.getRules()) {
            incorrect.addAll(rule.getCheckFunction().apply(sudoku));
        }
        return incorrect;
    }
}
