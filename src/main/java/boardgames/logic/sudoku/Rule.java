package boardgames.logic.sudoku;

import boardgames.logic.sudoku.util.Box;
import boardgames.logic.util.MoveDirection;
import boardgames.logic.util.Position;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum Rule {

    COLUMNS(0, "Unique digits in column", Rule::checkColumns),
    ROWS(1, "Unique digits in row", Rule::checkRows),
    BOXES(2, "Unique digits in box", Rule::checkBoxes),
    DIAGONALS(3, "Unique digits in diagonal", Rule::checkDiagonals),
    KNIGHTS_MOVE(4, "Knights-move rule", Rule::checkKnightsMove),
    KINGS_MOVE(5, "Kings-move rule", Rule::checkKingsMove),
    QUEENS_MOVE(6, "9 Queens-move rule", Rule::checkQueensMove),
    NON_CONSECUTIVE(7, "No consecutive digits", Rule::checkConsecutive);

    private final int index;

    private final String name;

    private final Function<Sudoku, List<SudokuCell>> checkFunction;

    Rule(int index, String name, Function<Sudoku, List<SudokuCell>> checkFunction) {
        this.index = index;
        this.name = name;
        this.checkFunction = checkFunction;
    }

    private static List<SudokuCell> checkColumns(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (List<SudokuCell> col : sudoku.getColumns()) {
            duplicates.addAll(getDuplicates(col));
        }
        return duplicates;
    }

    private static List<SudokuCell> checkRows(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (List<SudokuCell> row : sudoku.getRows()) {
            duplicates.addAll(getDuplicates(row));
        }
        return duplicates;
    }

    private static List<SudokuCell> checkBoxes(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (Box box : sudoku.getBoxes()) {
            duplicates.addAll(getDuplicates(box.getPositions().stream()
                    .map(sudoku::getCell)
                    .collect(Collectors.toList())));
        }
        return duplicates;
    }

    private static List<SudokuCell> checkDiagonals(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (List<SudokuCell> diagonal : sudoku.getDiagonals()) {
            duplicates.addAll(getDuplicates(diagonal));
        }
        return duplicates;
    }

    private static List<SudokuCell> checkKnightsMove(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                Position position = new Position(row, col);
                SudokuCell current = sudoku.getCell(position);
                boolean duplicate = getKnightsMovePositions(position).stream()
                        .map(sudoku::getCell)
                        .anyMatch((cell) -> Objects.equals(cell.getFinalDigit(), current.getFinalDigit()));
                if (duplicate) {
                    duplicates.add(current);
                }
            }
        }
        return duplicates;
    }

    private static List<Position> getKnightsMovePositions(Position position) {
        return MoveDirection.getKnightsMoves().stream()
                .map(position::move)
                .filter((p) -> p.valid(0, 9))
                .collect(Collectors.toList());
    }

    private static List<SudokuCell> checkKingsMove(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                Position position = new Position(row, col);
                SudokuCell current = sudoku.getCell(position);
                boolean duplicate = getKingsMoves(position).stream()
                        .map(sudoku::getCell)
                        .anyMatch((cell) -> Objects.equals(cell.getFinalDigit(), current.getFinalDigit()));
                if (duplicate) {
                    duplicates.add(current);
                }
            }
        }
        return duplicates;
    }

    private static List<Position> getKingsMoves(Position position) {
        return MoveDirection.getAllMoves().stream()
                .map(position::move)
                .filter((p) -> p.valid(0, 9))
                .collect(Collectors.toList());
    }

    private static List<SudokuCell> checkQueensMove(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                Position position = new Position(row, col);
                SudokuCell current = sudoku.getCell(position);
                if (Objects.equals(current.getFinalDigit(), 9)) {
                    List<Position> diagonals = new ArrayList<>();
                    Function<Position, Boolean> condition = (p) -> p.valid(0, 9);
                    diagonals.addAll(position.moveUntil(MoveDirection.TOP_LEFT, condition));
                    diagonals.addAll(position.moveUntil(MoveDirection.TOP_RIGHT, condition));
                    diagonals.addAll(position.moveUntil(MoveDirection.BOTTOM_LEFT, condition));
                    diagonals.addAll(position.moveUntil(MoveDirection.BOTTOM_RIGHT, condition));
                    boolean duplicate = diagonals.stream()
                            .map(sudoku::getCell)
                            .anyMatch((c) -> Objects.equals(c.getFinalDigit(), 9));
                    if (duplicate) {
                        duplicates.add(current);
                    }
                }
            }
        }
        return duplicates;
    }

    private static List<SudokuCell> checkConsecutive(Sudoku sudoku) {
        List<SudokuCell> duplicates = new ArrayList<>();
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                Position position = new Position(row, col);
                SudokuCell current = sudoku.getCell(position);
                Integer digit = current.getFinalDigit();
                if (digit != null) {
                    boolean consecutive = MoveDirection.getAllOrthogonal().stream()
                            .map(position::move)
                            .map(sudoku::getCell)
                            .filter((c) -> c.getFinalDigit() != null)
                            .anyMatch((c) -> Math.abs(digit - c.getFinalDigit()) == 1);
                    if (consecutive) {
                        duplicates.add(current);
                    }
                }
            }
        }
        return duplicates;
    }

    private static List<SudokuCell> getDuplicates(List<SudokuCell> cells) {
        Set<Integer> uniques = new HashSet<>();
        Set<Integer> duplicates = cells.stream()
                .map(SudokuCell::getFinalDigit)
                .filter(e -> e != null && !uniques.add(e))
                .collect(Collectors.toSet());
        return cells.stream()
                .filter((sudokuCell -> duplicates.contains(sudokuCell.getFinalDigit())))
                .collect(Collectors.toList());
    }

    public String getName() {
        return this.name;
    }

    @JsonValue
    public int getIndex() {
        return this.index;
    }

    public Function<Sudoku, List<SudokuCell>> getCheckFunction() {
        return this.checkFunction;
    }


    @Override
    public String toString() {
        return getName();
    }
}
