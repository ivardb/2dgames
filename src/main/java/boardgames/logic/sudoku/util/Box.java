package boardgames.logic.sudoku.util;

import boardgames.logic.util.Position;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Class to represent boxes of positions.
 */
@Getter
@EqualsAndHashCode
public class Box {

    private final Set<Position> positions;

    /**
     * Should only be used for deserialization.
     */
    public Box() {
        positions = new HashSet<>();
    }

    public Box(Set<Position> positions) {
        this.positions = positions.stream().map(Position::copy).collect(Collectors.toSet());
    }

    /**
     * Gets the boxes for a classic sudoku.
     * Can be used to draw the boxes.
     * @return List of boxes for a classic sudoku.
     */
    public static Set<Box> getDefaultSudokuBoxes() {
        Set<Box> boxes = new HashSet<>();
        for (int boxNum = 0; boxNum < 9; boxNum++) {
            int startRow = (boxNum / 3) * 3;
            int startCol = (boxNum % 3) * 3;
            Set<Position> positions = new HashSet<>();
            for (int row = startRow; row < startRow + 3; row++) {
                for (int col = startCol; col < startCol + 3; col++) {
                    positions.add(new Position(row, col));
                }
            }
            boxes.add(new Box(positions));
        }
        return boxes;
    }
}
