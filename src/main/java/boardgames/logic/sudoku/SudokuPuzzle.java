package boardgames.logic.sudoku;

import boardgames.gui.sudoku.util.serialization.ImprovedObjectMapper;
import boardgames.logic.sudoku.events.PuzzleUpdateEvent;
import boardgames.logic.sudoku.events.listeners.PuzzleUpdateListener;
import boardgames.logic.sudoku.util.Box;
import boardgames.logic.util.Position;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SudokuPuzzle {

    @JsonIgnore
    private Set<PuzzleUpdateListener> listeners;

    private String filename;

    private String name;

    private String description;

    private Integer[][] sudokuDigits;

    private Color[][] colors;

    private Set<Box> boxes;

    private Set<Rule> rules;

    /**
     * Constructor for JSON objectMapper.
     */
    public SudokuPuzzle() {
        listeners = new HashSet<>();
        boxes = new HashSet<>();
        rules = new HashSet<>();
        name = "";
        description = "";
        colors = getDefaultColors();
    }

    /**
     * Creates a sudoku puzzle.
     * @param filename Where to store it.
     * @param name The puzzle name.
     * @param description A description of the puzzle.
     * @param sudokuDigits The digits of the puzzle.
     * @param boxes The boxes to display.
     * @param rules The rules to check.
     */
    public SudokuPuzzle(String filename,
                        String name,
                        String description,
                        Integer[][] sudokuDigits,
                        Color[][] colors,
                        Set<Box> boxes,
                        Set<Rule> rules) {
        this.listeners = new HashSet<>();
        this.filename = filename;
        this.name = name;
        this.description = description;
        this.sudokuDigits = sudokuDigits;
        this.colors = colors;
        this.boxes = boxes;
        this.rules = rules;
    }

    /**
     * Enters a digit into the puzzle.
     * @param position The position to enter it.
     * @param digit The digit to enter.
     * @param activeMode The mode to use.
     */
    public void enterDigit(Position position, int digit, SudokuGame.Mode activeMode) {
        if (activeMode.equals(SudokuGame.Mode.FINAL)) {
            sudokuDigits[position.getRow()][position.getCol()] = digit;
        } else if (activeMode.equals(SudokuGame.Mode.COLORING)) {
            colors[position.getRow()][position.getCol()] = parseColor(digit);
        }
        notifyListeners();
    }

    /**
     * Deletes either the digit or the color.
     * @param position The position to delete.
     */
    public void delete(Position position) {
        Integer current = sudokuDigits[position.getRow()][position.getCol()];
        if (current != null) {
            sudokuDigits[position.getRow()][position.getCol()] = null;
            notifyListeners();
        } else {
            colors[position.getRow()][position.getCol()] = null;
        }
    }

    public void addPuzzleUpdateListener(PuzzleUpdateListener listener) {
        listeners.add(listener);
    }

    private void notifyListeners() {
        for (PuzzleUpdateListener listener : listeners) {
            listener.onUpdate(new PuzzleUpdateEvent(this));
        }
    }

    private Color parseColor(int digit) {
        switch (digit) {
            case 2:
                return new Color(41, 156, 71);
            case 3:
                return new Color(56, 176, 209);
            case 4:
                return new Color(212, 212, 55);
            case 5:
                return new Color(222, 149, 24);
            case 6:
                return new Color(255, 175, 175);
            case 7:
                return new Color(186, 21, 191);
            case 8:
                return new Color(128, 128, 128);
            case 9:
                return new Color(0, 0, 0);
            default:
                return new Color(255, 255, 255);
        }
    }

    /**
     * Creates a classic sudoku that is empty.
     * @return A sudoku puzzle.
     */
    public static SudokuPuzzle defaultEmptyClassicSudoku() {
        String filename = "puzzles/" + UUID.randomUUID() + ".txt";
        String name = "";
        String description = "";
        Integer[][] digits = new Integer[9][9];
        Color[][] colors = getDefaultColors();
        Set<Box> boxes = Box.getDefaultSudokuBoxes();
        Set<Rule> rules = Set.of(Rule.BOXES, Rule.COLUMNS, Rule.ROWS);
        return new SudokuPuzzle(filename, name, description, digits, colors, boxes, rules);
    }

    /**
     * Creates a sudoku puzzle from a file.
     * @param file The file to use.
     * @return The puzzle.
     */
    public static SudokuPuzzle fromFile(File file) {
        ObjectMapper objectMapper = new ImprovedObjectMapper();
        try {
            return objectMapper.readValue(file, SudokuPuzzle.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Color[][] getDefaultColors() {
        Color[][] colors = new Color[9][9];
        for (Color[] color : colors) {
            Arrays.fill(color, Color.white);
        }
        return colors;
    }

    public void setListeners(Set<PuzzleUpdateListener> listeners) {
        this.listeners = listeners;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSudokuDigits(Integer[][] sudokuDigits) {
        this.sudokuDigits = sudokuDigits;
        notifyListeners();
    }

    public void setBoxes(Set<Box> boxes) {
        this.boxes = boxes;
        notifyListeners();
    }

    public void setRules(Set<Rule> rules) {
        this.rules = rules;
        notifyListeners();
    }

    public void setRandomFilename() {
        filename = "puzzles/" + UUID.randomUUID() + ".txt";
    }
}
