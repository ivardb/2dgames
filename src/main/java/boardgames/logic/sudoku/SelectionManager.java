package boardgames.logic.sudoku;

import boardgames.gui.sudoku.SudokuLabel;
import boardgames.logic.sudoku.events.SelectionUpdateEvent;
import boardgames.logic.sudoku.events.listeners.SelectionUpdateListener;
import boardgames.logic.util.MoveDirection;
import boardgames.logic.util.Position;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;

public class SelectionManager {

    private final Set<SelectionUpdateListener> listeners;

    @Getter
    private final Set<Position> selected;

    private Position lastSelected;

    private boolean selecting;

    /**
     * Creates a selection manager.
     */
    public SelectionManager() {
        selected = new HashSet<>();
        listeners = new HashSet<>();
    }

    /**
     * Resets the selection to the given position and starts the selection process.
     * @param position The position to select.
     */
    public void startSelecting(Position position) {
        selected.clear();
        selected.add(position);
        lastSelected = position;
        selecting = true;
        notifyListeners();
    }

    /**
     * Adds the position to the selection.
     * @param position The position to add to the selection.
     */
    public void continueSelecting(Position position) {
        selected.add(position);
        lastSelected = position;
        selecting = true;
        notifyListeners();
    }

    /**
     * Expands the selection in the given movement direction.
     * @param direction The direction to move in and add to the selection.
     */
    public void expandSelection(MoveDirection direction) {
        if (lastSelected != null) {
            Position newPosition = lastSelected.move(direction);
            if (newPosition.valid(0, 9)) {
                selected.add(newPosition);
                lastSelected = newPosition;
                notifyListeners();
            }
        }
    }

    /**
     * Method to process the mouse events when a new cell is entered.
     * @param position The position of the entered cell.
     */
    public void enterCell(Position position) {
        if (selecting) {
            selected.add(position);
            lastSelected = position;
            notifyListeners();
        }
    }

    /**
     * Method that stops the selecting process.
     */
    public void stopSelecting() {
        selecting = false;
    }

    /**
     * Clear the selection and move in the direction of the last selected square
     * and select that one.
     * @param direction The direction to move in.
     */
    public void move(MoveDirection direction) {
        if (lastSelected != null) {
            Position newPosition = lastSelected.move(direction);
            if (newPosition.valid(0, 9)) {
                selected.clear();
                lastSelected = newPosition;
                selected.add(newPosition);
                notifyListeners();
            }
        }
    }

    public void clearSelection() {
        selected.clear();
        notifyListeners();
    }

    /**
     * Adds a new listener.
     * @param listener The listener to add.
     */
    public void addSelectionUpdateListener(SelectionUpdateListener listener) {
        listeners.add(listener);
    }

    /**
     * Notify listeners that the selection has changed.
     */
    private void notifyListeners() {
        for (SelectionUpdateListener listener : listeners) {
            listener.onUpdate(new SelectionUpdateEvent(this));
        }
    }

    /**
     * Gets a mouse listener for updating the selection of this object.
     * @return A configured mouse listener.
     */
    public MouseListener getMouseListener() {
        return new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isControlDown()) {
                    SudokuLabel sudokuLabel = (SudokuLabel) e.getSource();
                    continueSelecting(sudokuLabel.getPosition());
                } else {
                    SudokuLabel sudokuLabel = (SudokuLabel) e.getSource();
                    startSelecting(sudokuLabel.getPosition());
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                stopSelecting();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SudokuLabel sudokuLabel = (SudokuLabel) e.getSource();
                enterCell(sudokuLabel.getPosition());
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        };
    }
}
