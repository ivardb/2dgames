package boardgames.logic.util;

import java.util.Arrays;
import java.util.List;
import lombok.Getter;

@Getter
public class MoveDirection {

    public final static MoveDirection LEFT = new MoveDirection(0, -1);
    public final static MoveDirection TOP_LEFT = new MoveDirection(-1, -1);
    public final static MoveDirection BOTTOM_LEFT = new MoveDirection(1, -1);
    public final static MoveDirection RIGHT = new MoveDirection(0, 1);
    public final static MoveDirection TOP_RIGHT = new MoveDirection(-1, 1);
    public final static MoveDirection BOTTOM_RIGHT = new MoveDirection(1, 1);
    public final static MoveDirection UP = new MoveDirection(-1, 0);
    public final static MoveDirection DOWN = new MoveDirection(1, 0);

    private final int deltaRow;

    private final int deltaCol;

    public MoveDirection(int deltaRow, int deltaCol) {
        this.deltaRow = deltaRow;
        this.deltaCol = deltaCol;
    }

    public static List<MoveDirection> getKnightsMoves() {
        return Arrays.asList(new MoveDirection(1, 2),
                new MoveDirection(1, -2),
                new MoveDirection(2, 1),
                new MoveDirection(2, -1),
                new MoveDirection(-1, 2),
                new MoveDirection(-1, -2),
                new MoveDirection(-2, 1),
                new MoveDirection(-2, -1));
    }

    public static List<MoveDirection> getAllMoves() {
        return Arrays.asList(TOP_LEFT, TOP_RIGHT, UP, LEFT, RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, DOWN);
    }

    public static List<MoveDirection> getAllOrthogonal() {
        return Arrays.asList(LEFT, RIGHT, UP, DOWN);
    }
}
