package boardgames.logic.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Position {

    private final int row;

    private final int col;

    /**
     * Should only be used for deserialization.
     */
    public Position() {
        row = 0;
        col = 0;
    }

    /**
     * Returns a new Position moved in the movement direction.
     * @param direction The direction to move in.
     * @return A new position.
     */
    public Position move(MoveDirection direction) {
        int newRow = row + direction.getDeltaRow();
        int newCol = col + direction.getDeltaCol();
        return new Position(newRow, newCol);
    }

    /**
     * Keeps moving the position while the condition is true.
     * @param direction The direction to move in.
     * @param condition The condition to keep moving.
     * @return A list of all of the encountered positions.
     */
    public List<Position> moveUntil(MoveDirection direction, Function<Position, Boolean> condition) {
        List<Position> positions = new ArrayList<>();
        Position oldPosition = this;
        while (true) {
            Position newPosition = oldPosition.move(direction);
            if (condition.apply(newPosition)) {
                positions.add(newPosition);
                oldPosition = newPosition;
            } else {
                break;
            }
        }
        return positions;
    }

    public Position copy() {
        return new Position(getRow(), getCol());
    }

    /**
     * Checks if both the row and col are within the boundaries.
     * @param min Minimum boundary inclusive.
     * @param max Maximum boundary exclusive.
     * @return If both the row and col are within the boundaries.
     */
    public boolean valid(int min, int max) {
        return row >= min && row < max && col >= min && col < max;
    }

    @Override
    public int hashCode() {
        return 100 * row + col;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Position) {
            Position position = (Position) other;
            return position.getCol() == this.getCol() && position.getRow() == this.getRow();
        }
        return false;
    }
}
