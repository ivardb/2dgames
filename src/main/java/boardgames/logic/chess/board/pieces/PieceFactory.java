package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.ChessPosition;
import java.util.ArrayList;
import java.util.List;

public class PieceFactory {

    /**
     * Constructs a list of normal boardgames.logic.chess pieces in the correct positions.
     * @return A list of pieces.
     */
    public List<Piece> constructPieces() {
        List<Piece> pieces = new ArrayList<>();
        //Create white pieces
        int color = 1;
        pieces.add(new Rook(color, new ChessPosition("A1")));
        pieces.add(new Knight(color, new ChessPosition("B1")));
        pieces.add(new Bishop(color, new ChessPosition("C1")));
        pieces.add(new Queen(color, new ChessPosition("D1")));
        pieces.add(new King(color, new ChessPosition("E1")));
        pieces.add(new Bishop(color, new ChessPosition("F1")));
        pieces.add(new Knight(color, new ChessPosition("G1")));
        pieces.add(new Rook(color, new ChessPosition("H1")));

        for (int i = 0; i < 8; i++) {
            pieces.add(new Pawn(color, new ChessPosition(i, 1)));
        }

        //Create black pieces
        color = -1;
        pieces.add(new Rook(color, new ChessPosition("A8")));
        pieces.add(new Knight(color, new ChessPosition("B8")));
        pieces.add(new Bishop(color, new ChessPosition("C8")));
        pieces.add(new Queen(color, new ChessPosition("D8")));
        pieces.add(new King(color, new ChessPosition("E8")));
        pieces.add(new Bishop(color, new ChessPosition("F8")));
        pieces.add(new Knight(color, new ChessPosition("G8")));
        pieces.add(new Rook(color, new ChessPosition("H8")));

        for (int i = 0; i < 8; i++) {
            pieces.add(new Pawn(color, new ChessPosition(i, 6)));
        }
        return pieces;
    }
}
