package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.ChessPosition;
import java.util.List;

public class Bishop extends Piece {

    public Bishop(int color, ChessPosition position) {
        super(color, 'B', position);
    }

    @Override
    public List<ChessPosition> getMoves() {
        return getValidDiagonalMoves();
    }
}
