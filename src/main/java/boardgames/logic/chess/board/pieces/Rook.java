package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.ChessPosition;
import java.util.List;

public class Rook extends Piece {

    public Rook(int color, ChessPosition position) {
        super(color, 'R', position);
    }

    @Override
    public List<ChessPosition> getMoves() {
        return getValidLineMoves();
    }

    @Override
    public boolean move(ChessPosition newPosition) {
        if (!getValidMoves().contains(newPosition)) {
            return false;
        }
        //Clear old location
        ChessPosition oldPosition = getPosition();
        int oldRow = oldPosition.getRow();
        int oldCol = oldPosition.getColumn();
        getBoard().setPiece(null, oldRow, oldCol);

        //Clear piece in new position
        int newRow = newPosition.getRow();
        int newCol = newPosition.getColumn();
        Piece oldPiece = getPiece(newRow, newCol);
        if (oldPiece != null) {
            getBoard().removePiece(oldPiece);
        }
        //Add piece to new location
        getBoard().setPiece(this, newRow, newCol);
        setPosition(newPosition);

        getBoard().clearPawnDoubleMoves();
        if (getColor() == 1) {
            if (oldPosition.getPosition().equals("A1")) {
                getBoard().getGame().setWhiteCanCastleA(false);
            } else if (oldPosition.getPosition().equals("H1")) {
                getBoard().getGame().setWhiteCanCastleH(false);
            }
        } else {
            if (oldPosition.getPosition().equals("A8")) {
                getBoard().getGame().setBlackCanCastleA(false);
            } else if (oldPosition.getPosition().equals("H8")) {
                getBoard().getGame().setBlackCanCastleH(false);
            }
        }
        return true;
    }
}
