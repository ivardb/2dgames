package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.ChessPosition;
import java.util.List;

public class Queen extends Piece {

    public Queen(int color, ChessPosition position) {
        super(color, 'Q', position);
    }

    @Override
    public List<ChessPosition> getMoves() {
        List<ChessPosition> validMoves = getValidLineMoves();
        validMoves.addAll(getValidDiagonalMoves());
        return validMoves;
    }
}
