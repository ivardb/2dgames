package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.ChessPosition;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class King extends Piece {

    public King(int color, ChessPosition position) {
        super(color, 'K', position);
    }

    @Override
    public List<ChessPosition> getValidMoves() {
        List<ChessPosition> validMoves = getMoves().stream()
                .filter((move) -> !getBoard().wouldResultInCheck(this, move))
                .collect(Collectors.toList());
        validMoves.addAll(getCastleMoves());
        return validMoves;
    }

    @Override
    public List<ChessPosition> getMoves() {
        //Normal moves
        int[][] directions = new int[][]{{-1, -1}, {-1, 0}, {-1, 1}, {0, 1},
                {1, 1}, {1, 0}, {1, -1}, {0, -1}};

        return getValidDirectionalMoves(directions);
    }

    private List<ChessPosition> getCastleMoves() {
        List<ChessPosition> validMoves = new ArrayList<>();
        List<ChessPosition> lineMoves = getValidLineMoves();
        if (getColor() == 1) {
            if (getBoard().getGame().isWhiteCanCastleA()
                    && lineMoves.contains(new ChessPosition("B1"))) {
                if (!getBoard().wouldResultInCheck(this, new ChessPosition("D1"))
                        && !getBoard().wouldResultInCheck(this, new ChessPosition("C1"))) {
                    validMoves.add(new ChessPosition("C1"));
                }
            }
            if (getBoard().getGame().isWhiteCanCastleH()
                    && lineMoves.contains(new ChessPosition("G1"))) {
                if (!getBoard().wouldResultInCheck(this, new ChessPosition("F1"))
                        && !getBoard().wouldResultInCheck(this, new ChessPosition("G1"))) {
                    validMoves.add(new ChessPosition("G1"));
                }
            }
        } else {
            if (getBoard().getGame().isBlackCanCastleA()
                    && lineMoves.contains(new ChessPosition("B8"))) {
                if (!getBoard().wouldResultInCheck(this, new ChessPosition("D8"))
                        && !getBoard().wouldResultInCheck(this, new ChessPosition("C8"))) {
                    validMoves.add(new ChessPosition("C8"));
                }
            }
            if (getBoard().getGame().isBlackCanCastleH()
                    && lineMoves.contains(new ChessPosition("G8"))) {
                if (!getBoard().wouldResultInCheck(this, new ChessPosition("F8"))
                        && !getBoard().wouldResultInCheck(this, new ChessPosition("G8"))) {
                    validMoves.add(new ChessPosition("G8"));
                }
            }
        }
        return validMoves;
    }

    @Override
    public boolean move(ChessPosition newPosition) {
        if (!getValidMoves().contains(newPosition)) {
            return false;
        }
        if (Math.abs(newPosition.getColumn() - getPosition().getColumn()) == 2) {
            return castle(newPosition);
        }
        //Clear old location
        int oldRow = getPosition().getRow();
        int oldCol = getPosition().getColumn();
        getBoard().setPiece(null, oldRow, oldCol);

        //Clear piece in new position
        int newRow = newPosition.getRow();
        int newCol = newPosition.getColumn();
        Piece oldPiece = getPiece(newRow, newCol);
        if (oldPiece != null) {
            getBoard().removePiece(oldPiece);
        }
        //Add piece to new location
        getBoard().setPiece(this, newRow, newCol);
        setPosition(newPosition);

        getBoard().clearPawnDoubleMoves();
        if (getColor() == 1) {
            getBoard().getGame().setWhiteCanCastleA(false);
            getBoard().getGame().setWhiteCanCastleH(false);
        } else {
            getBoard().getGame().setBlackCanCastleA(false);
            getBoard().getGame().setBlackCanCastleH(false);
        }
        return true;
    }

    private boolean castle(ChessPosition newPosition) {
        //Clear old location
        int oldRow = getPosition().getRow();
        int oldCol = getPosition().getColumn();
        getBoard().setPiece(null, oldRow, oldCol);

        int newCol = newPosition.getColumn();

        getBoard().setPiece(this, oldRow, newCol);
        setPosition(newPosition);
        Piece rook;
        if (newCol < oldCol) {
            rook = getPiece(oldRow, 0);
            getBoard().setPiece(null, oldRow, 0);
            getBoard().setPiece(rook, oldRow, 3);
            rook.setPosition(new ChessPosition(3, oldRow));
        } else {
            rook = getPiece(oldRow, 7);
            getBoard().setPiece(null, oldRow, 7);
            getBoard().setPiece(rook, oldRow, 5);
            rook.setPosition(new ChessPosition(5, oldRow));
        }

        if (getColor() == 1) {
            getBoard().getGame().setWhiteCanCastleA(false);
            getBoard().getGame().setWhiteCanCastleH(false);
        } else {
            getBoard().getGame().setBlackCanCastleA(false);
            getBoard().getGame().setBlackCanCastleH(false);
        }
        return true;
    }
}
