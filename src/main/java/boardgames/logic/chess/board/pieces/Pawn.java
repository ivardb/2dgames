package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.ChessPosition;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

//TODO: implement promotion
@Setter
@Getter
public class Pawn extends Piece {

    private boolean hasDoubleMoved;

    public Pawn(int color, ChessPosition position) {
        super(color, 'P', position);
        hasDoubleMoved = false;
    }

    @Override
    public List<ChessPosition> getMoves() {
        int curRow = getPosition().getRow();
        int curCol = getPosition().getColumn();

        //Check for movement to the front
        List<ChessPosition> validMoves = new ArrayList<>();
        if (getPiece(curRow + getColor(), curCol) == null) {
            validMoves.add(new ChessPosition(curCol, curRow + getColor()));
            if (curRow == getStartLine(getColor())
                    && getPiece(curRow + 2 * getColor(), curCol) == null) {
                validMoves.add(new ChessPosition(curCol, curRow + 2 * getColor()));
            }
        }

        //Check for hits
        if (curCol != 0) {
            Piece potentialHit = getPiece(curRow + getColor(), curCol - 1);
            if (potentialHit != null && potentialHit.getColor() != getColor()) {
                validMoves.add(potentialHit.getPosition());
            }
        }
        if (curCol != 7) {
            Piece potentialHit = getPiece(curRow + getColor(), curCol + 1);
            if (potentialHit != null && potentialHit.getColor() != getColor()) {
                validMoves.add(potentialHit.getPosition());
            }
        }

        //Check for en passant
        if (curCol != 0) {
            Piece potentialPawn = getPiece(curRow, curCol - 1);
            if (potentialPawn != null && potentialPawn.getColor() != getColor()
                    && potentialPawn instanceof Pawn) {
                Pawn pawn = (Pawn) potentialPawn;
                if (pawn.hasDoubleMoved) {
                    validMoves.add(new ChessPosition(curCol - 1, curRow + getColor()));
                }
            }
        }

        if (curCol != 7) {
            Piece potentialPawn = getPiece(curRow, curCol + 1);
            if (potentialPawn != null && potentialPawn.getColor() != getColor()
                    && potentialPawn instanceof Pawn) {
                Pawn pawn = (Pawn) potentialPawn;
                if (pawn.hasDoubleMoved) {
                    validMoves.add(new ChessPosition(curCol + 1, curRow + getColor()));
                }
            }
        }
        return validMoves;
    }

    @Override
    public boolean move(ChessPosition newPosition) {
        if (!getValidMoves().contains(newPosition)) {
            return false;
        }
        //Clear old location
        int oldRow = getPosition().getRow();
        int oldCol = getPosition().getColumn();
        getBoard().setPiece(null, oldRow, oldCol);

        //Clear piece in new position
        int newRow = newPosition.getRow();
        int newCol = newPosition.getColumn();
        Piece oldPiece = getPiece(newRow, newCol);
        if (oldPiece != null) {
            getBoard().removePiece(oldPiece);
        }
        //Add piece to new location
        getBoard().setPiece(this, newRow, newCol);
        setPosition(newPosition);

        //Check for pawn double move.
        if (Math.abs(oldRow - newRow) == 1 && Math.abs(oldCol - newCol) == 1) {
            Piece potentialHitPawn = getPiece(oldRow, newCol);
            if (potentialHitPawn instanceof Pawn) {
                Pawn pawn = (Pawn) potentialHitPawn;
                if (pawn.isHasDoubleMoved()) {
                    getBoard().setPiece(null, oldRow, newCol);
                    getBoard().removePiece(potentialHitPawn);
                }
            }
        }
        getBoard().clearPawnDoubleMoves();
        if (Math.abs(oldRow - newRow) == 2) {
            setHasDoubleMoved(true);
        }
        if (newRow == getPromoteLine(getColor())) {
            getBoard().promote(this);
        }
        return true;
    }

    /**
     * Return starting row as array index.
     * @param color the color for the starting line.
     * @return The starting row.
     */
    private int getStartLine(int color) {
        if (color == 1) {
            return 1;
        } else {
            return 6;
        }
    }

    private int getPromoteLine(int color) {
        if (color == 1) {
            return 7;
        } else {
            return 0;
        }
    }
}
