package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.Board;
import boardgames.logic.chess.board.ChessPosition;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.ToString;

@Data
public abstract class Piece {

    //B:-1 or W: 1
    private int color;

    private char symbol;

    private ChessPosition position;

    @ToString.Exclude
    private Board board;

    /**
     * Constructs a boardgames.logic.chess piece.
     * @param color The color 1 for white, -1 for black.
     *              Used as indication for movement direction of pawns.
     * @param symbol The symbol to display.
     * @param position The position on the board.
     */
    public Piece(int color, char symbol, ChessPosition position) {
        this.color = color;
        this.symbol = symbol;
        this.position = position;
    }

    /**
     * Returns only valid moves, impossible to check yourself.
     * @return List of valid moves.
     */
    public List<ChessPosition> getValidMoves() {
        return getMoves().stream()
                .filter((move) -> !board.wouldResultInCheck(this, move))
                .collect(Collectors.toList());
    }

    /**
     * Gets list of possible moves, can check yourself.
     * @return List of possible moves.
     */
    public abstract List<ChessPosition> getMoves();

    /**
     * Gets list of valid moves in straight lines.
     * @return List of valid moves.
     */
    protected List<ChessPosition> getValidLineMoves() {
        int curRow = getPosition().getRow();
        int curCol = getPosition().getColumn();
        List<ChessPosition> validMoves = new ArrayList<>();
        //Moves going toward 8
        for (int i = curRow + 1; i < 8; i++) {
            if (moveObstructed(validMoves, i, curCol)) {
                break;
            }
        }

        //Moves going toward 0
        for (int i = curRow - 1; i >= 0; i--) {
            if (moveObstructed(validMoves, i, curCol)) {
                break;
            }
        }

        //Moves going toward H
        for (int i = curCol + 1; i < 8; i++) {
            if (moveObstructed(validMoves, curRow, i)) {
                break;
            }
        }

        //Moves going toward A
        for (int i = curCol - 1; i >= 0; i--) {
            if (moveObstructed(validMoves, curRow, i)) {
                break;
            }
        }
        return validMoves;
    }

    /**
     * Gets the valid moves on all diagonals.
     * @return List of valid moves.
     */
    protected List<ChessPosition> getValidDiagonalMoves() {
        int curRow = getPosition().getRow();
        int curCol = getPosition().getColumn();
        List<ChessPosition> validMoves = new ArrayList<>();

        getValidDiagonal(validMoves, curRow, curCol, 1, 1);
        getValidDiagonal(validMoves, curRow, curCol, 1, -1);
        getValidDiagonal(validMoves, curRow, curCol, -1, 1);
        getValidDiagonal(validMoves, curRow, curCol, -1, -1);

        return validMoves;
    }

    /**
     * Gets list of valid moves given list of possible directions.
     * @return List of valid moves.
     */
    protected List<ChessPosition> getValidDirectionalMoves(int[][] directions) {
        int curRow = getPosition().getRow();
        int curCol = getPosition().getColumn();
        List<ChessPosition> validMoves = new ArrayList<>();

        for (int[] direction : directions) {
            int i = direction[0] + curRow;
            int j = direction[1] + curCol;
            if (i >= 0 && i < 8 && j >= 0 && j < 8) {
                moveObstructed(validMoves, i, j);
            }
        }
        return validMoves;
    }

    /**
     * Goes diagonally in given direction from starting point, and gets valid moves.
     * @param validMoves List of valid moves.
     * @param startRow The start row.
     * @param startCol The start col.
     * @param changeRow The row change direction.
     * @param changeCol The col change direction.
     */
    private void getValidDiagonal(List<ChessPosition> validMoves, int startRow, int startCol,
                                  int changeRow, int changeCol) {
        int i = startRow + changeRow;
        int j = startCol + changeCol;

        while (i < 8 && i >= 0 && j < 8 && j >= 0) {
            if (moveObstructed(validMoves, i, j)) {
                break;
            }
            i += changeRow;
            j += changeCol;
        }
    }

    /**
     * Checks if move is obstructed, while updating validMoves list.
     * @param validMoves List of moves.
     * @param row The row.
     * @param col The column.
     * @return If obstructed.
     */
    private boolean moveObstructed(List<ChessPosition> validMoves, int row, int col) {
        Piece piece = getPiece(row, col);
        if (piece == null) {
            validMoves.add(new ChessPosition(col, row));
        } else {
            if (piece.getColor() != getColor()) {
                validMoves.add(new ChessPosition(col, row));
            }
            return true;
        }
        return false;
    }

    protected Piece getPiece(int row, int col) {
        return getBoard().getPiece(row, col);
    }

    /**
     * Moves piece, returns false if unsuccessful.
     * @param newPosition The new position
     * @return if successful
     */
    public boolean move(ChessPosition newPosition) {
        if (!getValidMoves().contains(newPosition)) {
            return false;
        }
        //Clear old location
        int oldRow = getPosition().getRow();
        int oldCol = getPosition().getColumn();
        getBoard().setPiece(null, oldRow, oldCol);

        //Clear piece in new position
        int newRow = newPosition.getRow();
        int newCol = newPosition.getColumn();
        Piece oldPiece = getPiece(newRow, newCol);
        if (oldPiece != null) {
            getBoard().removePiece(oldPiece);
        }
        //Add piece to new location
        getBoard().setPiece(this, newRow, newCol);
        setPosition(newPosition);

        board.clearPawnDoubleMoves();
        return true;
    }
}
