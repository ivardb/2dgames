package boardgames.logic.chess.board.pieces;

import boardgames.logic.chess.board.ChessPosition;
import java.util.List;

public class Knight extends Piece {

    public Knight(int color, ChessPosition position) {
        super(color, 'N', position);
    }

    @Override
    public List<ChessPosition> getMoves() {
        int[][] directions = new int[][]{{-2, -1}, {-2, 1}, {2, 1}, {2, -1},
                {1, -2}, {-1, -2}, {1, 2}, {-1, 2}};
        
        return getValidDirectionalMoves(directions);
    }
}
