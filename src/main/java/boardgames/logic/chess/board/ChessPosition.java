package boardgames.logic.chess.board;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class ChessPosition {

    private String position;

    /**
     * Constructs a boardgames.logic.chess position from the actual position string.
     * A1 - H8
     * @param position String position.
     */
    public ChessPosition(String position) {
        setPosition(position);
    }

    /**
     * Constructs a boardgames.logic.chess position from indexes.
     * @param column 0 - 7
     * @param row 0 - 7
     */
    public ChessPosition(int column, int row) {
        setPosition(column, row);
    }

    /**
     * Sets position using actual position.
     * @param position Valid boardgames.logic.chess position.
     */
    public void setPosition(@NonNull String position) {
        if (position.length() != 2) {
            throw new IllegalArgumentException("Not a valid boardgames.logic.chess position");
        }
        char letter = position.charAt(0);
        char number = position.charAt(1);
        if (letter < 'A' || letter > 'H') {
            throw new IllegalArgumentException("Not a valid column");
        }
        if (Character.isDigit(number)) {
            int num = Character.getNumericValue(number);
            if (num < 1 || num > 8) {
                throw new IllegalArgumentException("Not a valid row");
            }
        } else {
            throw new IllegalArgumentException("Not a valid row");
        }
        this.position = position;
    }

    /**
     * Sets boardgames.logic.chess position from indexes.
     * @param column 0 - 7
     * @param row 0 - 7
     */
    public void setPosition(int column, int row) {
        if (column < 0 || column > 7 || row < 0 || row > 7) {
            throw new IllegalArgumentException("Out of bounds");
        }
        position = ((char) ('A' + column)) + (row + 1 + "");
    }

    public String getPosition() {
        return position;
    }

    public int getRow() {
        return Character.getNumericValue(position.charAt(1)) - 1;
    }

    public int getColumn() {
        return position.charAt(0) - 'A';
    }
}
