package boardgames.logic.chess.board;

import boardgames.logic.chess.ChessGame;
import boardgames.logic.chess.board.pieces.Bishop;
import boardgames.logic.chess.board.pieces.Knight;
import boardgames.logic.chess.board.pieces.Pawn;
import boardgames.logic.chess.board.pieces.Piece;
import boardgames.logic.chess.board.pieces.PieceFactory;
import boardgames.logic.chess.board.pieces.Queen;
import boardgames.logic.chess.board.pieces.Rook;
import java.util.List;
import javax.swing.JOptionPane;
import lombok.Data;

@Data
public class Board {

    private List<Piece> pieces;

    private Piece[][] board;

    private ChessGame game;

    /**
     * Creates a board.
     * @param game The game.
     * @param pieceFactory Factory to generate pieces.
     */
    public Board(ChessGame game, PieceFactory pieceFactory) {
        this.game = game;
        pieces = pieceFactory.constructPieces();
        initializeBoard();
    }

    public Piece getPiece(int row, int column) {
        return board[row][column];
    }

    /**
     * Initializes the pieces in the correct board places.
     */
    public void initializeBoard() {
        board = new Piece[8][8];
        for (Piece piece : pieces) {
            int row = piece.getPosition().getRow();
            int col = piece.getPosition().getColumn();
            board[row][col] = piece;
            piece.setBoard(this);
        }
    }

    /**
     * Checks if a move would result in the player being checked.
     * @param piece The piece that would be moved.
     * @param newPosition Its new position.
     * @return if they would be checked.
     */
    public boolean wouldResultInCheck(Piece piece, ChessPosition newPosition) {
        ChessPosition oldPosition = piece.getPosition();
        int oldRow = oldPosition.getRow();
        int oldCol = oldPosition.getColumn();
        board[oldRow][oldCol] = null;

        int newRow = newPosition.getRow();
        int newCol = newPosition.getColumn();
        final Piece oldPiece = getPiece(newRow, newCol);
        if (oldPiece != null) {
            pieces.remove(oldPiece);
        }

        board[newRow][newCol] = piece;
        piece.setPosition(newPosition);

        final boolean isChecked = getChecked(piece.getColor());

        board[oldRow][oldCol] = piece;
        piece.setPosition(oldPosition);
        board[newRow][newCol] = oldPiece;
        if (oldPiece != null) {
            pieces.add(oldPiece);
        }
        return isChecked;
    }

    /**
     * Checks if given player is checked.
     * @param color The players color.
     * @return If checked.
     */
    public boolean getChecked(int color) {
        ChessPosition kingPosition = null;
        for (Piece piece : pieces) {
            if (piece.getColor() == color && piece.getSymbol() == 'K') {
                kingPosition = piece.getPosition();
            }
        }
        for (Piece piece : pieces) {
            if (piece.getColor() != color) {
                if (piece.getMoves().contains(kingPosition)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Resets pawn double move field.
     */
    public void clearPawnDoubleMoves() {
        for (Piece piece: pieces) {
            if (piece instanceof Pawn) {
                ((Pawn) piece).setHasDoubleMoved(false);
            }
        }
    }

    /**
     * Promotes the pawn to a queen.
     * @param pawn The pawn to be promoted.
     */
    public void promote(Pawn pawn) {
        String[] choices = {"Queen", "Rook", "Knight", "Bishop"};
        String choice = (String) JOptionPane.showInputDialog(null,
                "Promote to:",
                "Promotion",
                JOptionPane.QUESTION_MESSAGE,
                null,
                choices,
                choices[0]);
        Piece piece;
        if (choice == null || choice.equals(choices[0])) {
            piece = new Queen(pawn.getColor(), pawn.getPosition());
        } else if (choice.equals(choices[1])) {
            piece = new Rook(pawn.getColor(), pawn.getPosition());
        } else if (choice.equals(choices[2])) {
            piece = new Knight(pawn.getColor(), pawn.getPosition());
        } else {
            piece = new Bishop(pawn.getColor(), pawn.getPosition());
        }
        piece.setBoard(this);
        pieces.remove(pawn);
        pieces.add(piece);
        board[pawn.getPosition().getRow()][pawn.getPosition().getColumn()] = piece;
    }

    public void setPiece(Piece piece, int row, int col) {
        board[row][col] = piece;
    }

    public void removePiece(Piece piece) {
        pieces.remove(piece);
    }
}
