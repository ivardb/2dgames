package boardgames.logic.chess;

import boardgames.gui.chess.ChessBoardPanel;
import boardgames.gui.chess.ChessInformationPanel;
import boardgames.gui.chess.ChessPanel;
import boardgames.logic.chess.board.Board;
import boardgames.logic.chess.board.ChessPosition;
import boardgames.logic.chess.board.pieces.Piece;
import boardgames.logic.chess.board.pieces.PieceFactory;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ChessGame {

    private ChessPanel chessPanel;

    private Board board;

    private boolean whiteTurn;

    private Piece selected;

    private boolean whiteCanCastleA;

    private boolean whiteCanCastleH;

    private boolean blackCanCastleA;

    private boolean blackCanCastleH;

    private boolean checkmate;

    /**
     * Creates a boardgames.logic.chess game.
     * @param chessPanel The top level boardgames.logic.chess panel.
     */
    public ChessGame(ChessPanel chessPanel) {
        this.chessPanel = chessPanel;
        PieceFactory pieceFactory = new PieceFactory();
        this.board = new Board(this, pieceFactory);
        whiteTurn = true;
        whiteCanCastleA = true;
        whiteCanCastleH = true;
        blackCanCastleA = true;
        blackCanCastleH = true;
    }

    /**
     * Called for mouse events.
     * @param row Row of click event.
     * @param col Col of click event.
     */
    public void squareClicked(int row, int col) {
        if (checkmate) {
            return;
        }
        if (selected == null) {
            Piece piece = getBoard().getPiece(row, col);
            if (piece != null && ((piece.getColor() == 1) == whiteTurn)) {
                List<ChessPosition> validMoves = piece.getValidMoves();
                if (!validMoves.isEmpty()) {
                    getBoardPanel().displayValidMoves(validMoves);
                    selected = piece;
                }
            }
        } else {
            ChessPosition newPosition = new ChessPosition(col, row);
            if (selected.getValidMoves().contains(newPosition)) {
                if (selected.move(newPosition)) {
                    getBoardPanel().clearValidMoves();
                    getBoardPanel().updateLabels();
                    selected = null;
                    whiteTurn = !whiteTurn;
                    getInformationPanel().nextTurn(whiteTurn);
                    checkMate(whiteTurn ? 1 : -1);
                }
            } else {
                selected = null;
                getBoardPanel().clearValidMoves();
            }
        }
    }

    /**
     * Check mate for given color.
     * @param color The color
     */
    public void checkMate(int color) {
        List<Piece> pieces = new ArrayList<>(board.getPieces());
        for (Piece piece : pieces) {
            if (piece.getColor() == color && !piece.getValidMoves().isEmpty()) {
                return;
            }
        }
        String player = color == 1 ? "White player" : "Black player";
        System.out.println(player + " has been checkmated, press 'r' to reset game");
        checkmate = true;
    }

    private ChessBoardPanel getBoardPanel() {
        return chessPanel.getBoardPanel();
    }

    private ChessInformationPanel getInformationPanel() {
        return chessPanel.getInformationPanel();
    }

}
